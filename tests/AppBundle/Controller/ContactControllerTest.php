<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\Test\LoadContactData;
use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactCampaignSource;
use AppBundle\Entity\ContactGroup;
use AppBundle\Entity\File;

/**
 * Class ContactControllerTest
 *
 * @package AppBundle\Controller
 */
class ContactControllerTest extends AbstractControllerTest
{
    protected $url = '/api/contacts';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadContactData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 3, 3, 'firstName', 'John');
    }

    public function testFilters()
    {
        $result = $this->getList(null, ['email' => 'john*', 'phone' => '3464*', 'group_name' => 'group1', 'campaign_source_name' => 'facebook']);

        static::assertListResult($result, 1, 1, 'email', 'john.doe@email.com');
    }

    public function testSearch()
    {
        $result = $this->getList(null, ['query' => 'John']);

        static::assertListResult($result, 1, 1, 'email', 'john.doe@email.com');

        $result = $this->getList(null, ['query' => 'Doe']);

        static::assertListResult($result, 3, 3, 'email', 'john.doe@email.com');
    }

    public function testCreate()
    {

        $this->createItem([
            'firstName' => 'testFirstName',
            'lastName' => 'testLastName',
            'email' => 'testEmail@email.com',
            'budget' => 1111,
            'employer' => 'testEmployer',
            'phone' => 2345655435,
            'profession' => 'testProfession',
            'campaignSource' => $this->getObjectOf(ContactCampaignSource::class, ['name' => 'facebook'])->getId(),
            'groups' => [$this->getObjectOf(ContactGroup::class, ['name' => 'group1'])->getId()],
            'documents' => [$this->getObjectOf(File::class, ['name' => 'doc0.png'])->getId(),$this->getObjectOf(File::class, ['name' => 'doc1.png'])->getId()],
        ]);

        /** @var Contact $entity */
        $entity = $this->getObjectOf(Contact::class, ['firstName' => 'testFirstName']);
        static::assertEquals('testFirstName', $entity->getFirstName());
    }


    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['firstName' => 'testEditFirstName'], $id);

        /** @var Contact $entity */
        $entity = $this->getObjectOf(Contact::class, ['id' => $id]);
        static::assertEquals('testEditFirstName', $entity->getFirstName());
    }

    protected function getEntityName()
    {
        return Contact::class;
    }
}