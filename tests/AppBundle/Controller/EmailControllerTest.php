<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\Test\LoadContactData;
use AppBundle\DataFixtures\Test\LoadEmailData;
use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactCampaignSource;
use AppBundle\Entity\ContactGroup;
use AppBundle\Entity\Email;
use AppBundle\Entity\File;

/**
 * Class ContactControllerTest
 *
 * @package AppBundle\Controller
 */
class EmailControllerTest extends AbstractControllerTest
{
    protected $url = '/api/emails';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadEmailData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 2, 2, 'subject', 'testSubject1');
    }

    public function testCreate()
    {
        $this->createItem([
            'subject' => 'testSubject',
            'message' => 'testMessage',
            'recipients' => [['contact' => $this->getObjectOf(Contact::class, ['firstName' => 'John'])->getId()],['contact' => $this->getObjectOf(Contact::class, ['firstName' => 'Bob'])->getId()]],
            'attachments' => [$this->getObjectOf(File::class, ['name' => 'doc0.png'])->getId(),$this->getObjectOf(File::class, ['name' => 'doc1.png'])->getId()],
            'status' => 'New',
        ]);

        /** @var Email $entity */
        $entity = $this->getObjectOf(Email::class, ['subject' => 'testSubject']);
        static::assertEquals('testSubject', $entity->getSubject());
    }

    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['subject' => 'testSubjectUpdated'], $id);

        /** @var Email $entity */
        $entity = $this->getObjectOf(Email::class, ['id' => $id]);
        static::assertEquals('testSubjectUpdated', $entity->getSubject());
    }

    protected function getEntityName()
    {
        return Email::class;
    }
}