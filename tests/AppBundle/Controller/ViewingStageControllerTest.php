<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadAccessTokenData;
use AppBundle\DataFixtures\ORM\LoadViewingStagesData;
use AppBundle\Entity\Unit;
use AppBundle\Entity\ViewingStage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ViewingStageControllerTest
 *
 * @package AppBundle\Controller
 */
class ViewingStageControllerTest extends AbstractControllerTest
{
    protected $url = '/api/viewings-stages';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadAccessTokenData::class,
            LoadViewingStagesData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 7, 7, 'name', 'scheduled');
    }

    public function testCreate()
    {
        /** @var ViewingStage $finishStage */
        $finishStage = $this->getObjectOf(ViewingStage::class,['flowFlag' => ViewingStage::FINISH_STAGE]);

        $order = $finishStage->getOrder();

        $this->createItem([
            'name' => 'new_stage',
            'label' => 'New Stage',
        ]);

        /** @var ViewingStage $stage */
        $stage = $this->getObjectOf(ViewingStage::class, ['name' => 'new_stage']);

        static::assertEquals('new_stage', $stage->getName());
        static::assertEquals(true, $stage->isMiddle());
        static::assertEquals($order, $stage->getOrder());
        static::assertEquals($order + 1, $finishStage->getOrder());
    }


    public function testEdit()
    {
        /** @var ViewingStage $stage */
        $stage = $this->getObjectOf(ViewingStage::class, ['flowFlag' => ViewingStage::MIDDLE_STAGE]);
        $this->updateItem(['label' => 'Updated Label'], $stage->getId());

        /** @var ViewingStage $entity */
        $entity = $this->getObjectOf(ViewingStage::class, ['id' => $stage->getId()]);
        static::assertEquals('Updated Label', $entity->getLabel());
    }

    public function testReorder()
    {
        /** @var ViewingStage $finishStage */
        $finishStage = $this->getObjectOf(ViewingStage::class, ['flowFlag' => ViewingStage::FINISH_STAGE]);

        /** @var ViewingStage $stage */
        $stage = $this->getObjectOf(ViewingStage::class, ['order' => 2]);
        $this->updateItem(['order' => $finishStage->getOrder() - 1], $stage->getId());

        /** @var ViewingStage $entity */
        $entity = $this->getObjectOf(ViewingStage::class, ['id' => $stage->getId()]);
        static::assertEquals($finishStage->getOrder() - 1, $entity->getOrder());
    }

    /**
     * {@inheritdoc}
     */
    public function testDelete()
    {
        $object = $this->getObjectOf(ViewingStage::class, ['flowFlag' => ViewingStage::MIDDLE_STAGE]);
        $id = $object->getId();

        $this->getClient()
            ->request(Request::METHOD_DELETE, $this->getResourceUrl().'/'.$id, [], [], $this->headers);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $this->getClient()->getResponse()->getStatusCode());

        $result = $this->getClient()->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository($this->getEntityName())->find($id);

        $this->assertNull($result);

        return $this->getJsonResponse();
    }

    protected function getEntityName()
    {
        return ViewingStage::class;
    }
}