<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\Test\LoadPropertyData;

use AppBundle\Entity\BankInfo;

/**
 * Class PropertyControllerTest
 *
 * @package AppBundle\Controller
 */
class BankInfoControllerTest extends AbstractControllerTest
{
    protected $url = '/api/bank-info';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadPropertyData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 2, 2, 'name', 'Standard Chartered Bank');
    }

    public function testCreate()
    {
        $this->createItem([
            'name' => 'bankInfoName',
            'accountName' => 'bankInfoAccountName',
            'accountNumber' => 'bankInfoAccountNumber',
            'branchCode' => 'bankInfoBranchCode',
            'code' => 'bankInfoCode',
            'swift' => 'bankInfoSwift',
        ]);

        /** @var BankInfo $entity */
        $entity = $this->getObjectOf(BankInfo::class, ['name' => 'bankInfoName']);
        static::assertEquals('bankInfoName', $entity->getName());
    }

    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['name' => 'bankEditInfoName'], $id);

        /** @var BankInfo $entity */
        $entity = $this->getObjectOf(BankInfo::class, ['id' => $id]);
        static::assertEquals('bankEditInfoName', $entity->getName());
    }

    protected function getEntityName()
    {
        return BankInfo::class;
    }
}