<?php

namespace AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadTemplateData;
use AppBundle\DataFixtures\ORM\LoadTemplateParametersData;
use AppBundle\DataFixtures\Test\LoadEmailData;
use AppBundle\DataFixtures\Test\LoadLeasingData;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Leasing;
use AppBundle\Entity\LeasingType;
use AppBundle\Entity\Unit;
use AppBundle\Entity\ViewingStage;

/**
 * Class LeasingControllerTest
 *
 * @package AppBundle\Controller
 */
class LeasingControllerTest extends AbstractControllerTest
{
    protected $url = '/api/leasings';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadLeasingData::class,
            LoadTemplateData::class
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 1, 1, 'notes', 'test leasing');
    }

    public function testCreate()
    {
        /** @var LeasingType $type */
        $type = $this->getObjectOf(LeasingType::class, ['name' => 'NR']);

        /** @var Contact $contact */
        $contact = $this->getObjectOf(Contact::class, ['email' => 'bob.doe@email.com']);

        /** @var Unit $unit */
        $unit = $this->getObjectOf(Unit::class, ['name' => 'C']);

        $this->createItem([
            'type' => $type->getId(),
            'contact' => $contact->getId(),
            'unit' => $unit->getId(),
            'price' => 12500,
            'startDate' => (new \DateTime('+20 day'))->format('d.m.Y'),
            'payDay' => 20,
            'endDate' => (new \DateTime('+6 month'))->format('d.m.Y'),
            'securityDepositAmount' => 440000,
            'notes' => 'test leasing 12500'
        ]);

        /** @var Leasing $entity */
        $entity = $this->getObjectOf(Leasing::class, ['notes' => 'test leasing 12500']);
        static::assertEquals('test leasing 12500', $entity->getNotes());
    }


    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['price' => 43660], $id);

        /** @var Leasing $entity */
        $entity = $this->getObjectOf(Leasing::class, ['id' => $id]);
        static::assertEquals(43660, $entity->getPrice());
    }

    protected function getEntityName()
    {
        return Leasing::class;
    }
}