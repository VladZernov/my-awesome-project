<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\Test\LoadUnitData;
use AppBundle\Entity\Unit;

/**
 * Class UnitControllerTest
 *
 * @package AppBundle\Controller
 */
class UnitControllerTest extends AbstractControllerTest
{
    protected $url = '/api/units';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadUnitData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 3, 3, 'name', 'A');
    }

    public function testCreate()
    {

        $this->createItem([
            'name' => 'testName',
            'bedSize' => 'testSize',
            'doorCode' => '1234',
            'grossSize' => 1111,
            'modelPrice' => 1000,
            'targetPrice' => 1000,
            'notes' => 'testNOtes',
        ]);

        /** @var Unit $entity */
        $entity = $this->getObjectOf(Unit::class, ['name' => 'testName']);
        static::assertEquals('testName', $entity->getName());
    }


    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['name' => 'testEditName'], $id);

        /** @var Unit $entity */
        $entity = $this->getObjectOf(Unit::class, ['id' => $id]);
        static::assertEquals('testEditName', $entity->getName());
    }

    protected function getEntityName()
    {
        return Unit::class;
    }
}