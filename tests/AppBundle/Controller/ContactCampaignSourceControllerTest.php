<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadAccessTokenData;
use AppBundle\DataFixtures\ORM\LoadCampaignSourcesData;
use AppBundle\Entity\ContactCampaignSource;

/**
 * Class ContactCampaignSourceControllerTest
 *
 * @package AppBundle\Controller
 */
class ContactCampaignSourceControllerTest extends AbstractControllerTest
{
    protected $url = '/api/contacts-campaign-sources';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadAccessTokenData::class,
            LoadCampaignSourcesData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 5, 5, 'name', 'facebook');
    }

    public function testCreate()
    {
        $this->createItem([
            'name' => 'test campaign source',
        ]);

        /** @var ContactCampaignSource $entity */
        $entity = $this->getObjectOf(ContactCampaignSource::class, ['name' => 'test campaign source']);
        static::assertEquals('test campaign source', $entity->getName());
    }

    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['name' => 'updated campaign source'], $id);

        /** @var ContactCampaignSource $entity */
        $entity = $this->getObjectOf(ContactCampaignSource::class, ['id' => $id]);
        static::assertEquals('updated campaign source', $entity->getName());
    }

    protected function getEntityName()
    {
        return ContactCampaignSource::class;
    }
}