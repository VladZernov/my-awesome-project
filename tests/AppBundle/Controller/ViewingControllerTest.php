<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\Test\LoadViewingData;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Unit;
use AppBundle\Entity\Viewing;
use AppBundle\Entity\ViewingStage;

/**
 * Class ViewingControllerTest
 *
 * @package AppBundle\Controller
 */
class ViewingControllerTest extends AbstractControllerTest
{
    protected $url = '/api/viewings';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadViewingData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 3, 3, 'notes', 'testViewing1Note');
    }

    public function testCreate()
    {
        $this->createItem([
            'price' => 1200,
            'notes' => 'testViewingNotes',
            'date' => '20.05.2018',
            'stage' => $this->getObjectOf(ViewingStage::class, ['name' => 'scheduled'])->getId(),
            'unit' => $this->getObjectOf(Unit::class, ['name' => 'B'])->getId(),
            'contact' => $this->getObjectOf(Contact::class, ['firstName' => 'John'])->getId(),
        ]);

        /** @var Viewing $entity */
        $entity = $this->getObjectOf(Viewing::class, ['notes' => 'testViewingNotes']);
        static::assertEquals('testViewingNotes', $entity->getNotes());
    }


    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['notes' => 'testViewingNotes'], $id);

        /** @var Unit $entity */
        $entity = $this->getObjectOf(Viewing::class, ['id' => $id]);
        static::assertEquals('testViewingNotes', $entity->getNotes());
    }

    protected function getEntityName()
    {
        return Viewing::class;
    }
}