<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Requestum\ApiBundle\Tests\RestCrudTestCase;

/**
 * Class AbstractControllerTest
 *
 * @package AppBundle\Controller
 */
abstract class AbstractControllerTest extends RestCrudTestCase
{
    public function testFetch()
    {
        $this->getItem();
    }

    public function testDelete()
    {
        $this->deleteItem(null, Response::HTTP_NO_CONTENT, false);
    }
}