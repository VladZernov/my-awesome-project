<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadAccessTokenData;
use AppBundle\DataFixtures\ORM\LoadContactGroupData;
use AppBundle\DataFixtures\Test\LoadContactData;
use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactGroup;

/**
 * Class ContactGroupControllerTest
 *
 * @package AppBundle\Controller
 */
class ContactGroupControllerTest extends AbstractControllerTest
{
    protected $url = '/api/contacts-groups';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadAccessTokenData::class,
            LoadContactData::class,
            LoadContactGroupData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 3, 3, 'name', 'group1');
    }

    public function testCreate()
    {
        $this->createItem([
            'name' => 'test contact group',
            'contacts' => [
                $this->getObjectOf(Contact::class, ['email' => 'john.doe@email.com'])->getId(),
                $this->getObjectOf(Contact::class, ['email' => 'bob.doe@email.com'])->getId(),
            ]
        ]);

        /** @var ContactGroup $entity */
        $entity = $this->getObjectOf(ContactGroup::class, ['name' => 'test contact group']);
        static::assertEquals('test contact group', $entity->getName());
    }

    public function testEdit()
    {
        /** @var  $contact */
        $contact = $this->getObjectOf(Contact::class, ['email' => 'dale.doe@email.com']);

        $id = $this->getExistedObjectId();
        $this->updateItem(['contacts' => [
            $contact->getId(),
        ]], $id);

        /** @var ContactGroup $entity */
        $entity = $this->getObjectOf(ContactGroup::class, ['id' => $id]);
        static::assertContains($contact, $entity->getContacts()->toArray());
    }

    protected function getEntityName()
    {
        return ContactGroup::class;
    }
}