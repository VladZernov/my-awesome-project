<?php
namespace AppBundle\Controller;

use AppBundle\Entity\ContactCampaignSource;
use AppBundle\DataFixtures\Test\LoadViewingData;
use Requestum\ApiBundle\Tests\RestCrudTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StatisticsControllerTest
 *
 * @package AppBundle\Controller
 */
class StatisticsControllerTest extends RestCrudTestCase
{
    protected $url = '/api/statistics';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadViewingData::class,
        ]);
    }

    public function testCampaignSourcesStatistics()
    {
        $date = new \DateTime();

        $this->getClient()->request(Request::METHOD_GET, $this->getResourceUrl().'/campaign-sources', ['from' => $date->format('Y.m.01'), 'to' => $date->format('Y.m.t')], [], $this->headers);
        $this->assertEquals(Response::HTTP_OK, $this->getClient()->getResponse()->getStatusCode());

        $result = $this->getJsonResponse();

        $this->assertNotEmpty($result);
        $this->assertEquals('facebook', $result[0][0]['name']);
        $this->assertEquals('100', $result[0]['percentage']);
        $this->assertEquals('2000', $result[0]['sum']);
    }

    protected function getEntityName()
    {
        return ContactCampaignSource::class;
    }
}