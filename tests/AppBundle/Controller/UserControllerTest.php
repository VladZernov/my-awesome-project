<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadAccessTokenData;
use AppBundle\DataFixtures\ORM\LoadUserData;
use AppBundle\Entity\User;

/**
 * Class UserControllerTest
 *
 * @package AppBundle\Controller
 */
class UserControllerTest extends AbstractControllerTest
{
    protected $url = '/api/users';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadAccessTokenData::class,
            LoadUserData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 2, 2, 'email', 'alex@gmail.com');
    }

    public function testCreate()
    {

        $this->createItem([
            'email' => 'testEmail@email.com',
            'fullName' => 'testFullName',
            'plainPassword' => 123,
            'role' => User::ROLE_USER
        ]);

        /** @var User $entity */
        $entity = $this->getObjectOf(User::class, ['email' => 'testEmail@email.com']);
        static::assertEquals('testEmail@email.com', $entity->getEmail());
    }

    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['fullName' => 'updatedName'], $id);

        /** @var User $entity */
        $entity = $this->getObjectOf(User::class, ['id' => $id]);
        static::assertEquals('updatedName', $entity->getFullName());
    }

    protected function getEntityName()
    {
        return User::class;
    }
}