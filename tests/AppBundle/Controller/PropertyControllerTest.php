<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\Test\LoadLeasingData;
use AppBundle\DataFixtures\Test\LoadPropertyData;

use AppBundle\Entity\Property;

/**
 * Class PropertyControllerTest
 *
 * @package AppBundle\Controller
 */
class PropertyControllerTest extends AbstractControllerTest
{
    protected $url = '/api/property';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadPropertyData::class,
            LoadLeasingData::class
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 3, 3, 'name', 'Sparrow Hills');
    }

    public function testCreate()
    {
        $this->createItem([
            'name' => 'testBuildingName',
            'city' => 'testBuildingCity',
            'address' => 'testBuildingAddress',
            'zipcode' => 'testBuildingZipcode'
        ]);

        /** @var Property $entity */
        $entity = $this->getObjectOf(Property::class, ['name' => 'testBuildingName']);
        static::assertEquals('testBuildingName', $entity->getName());
    }

    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['name' => 'testEditBuildingName'], $id);

        /** @var Property $entity */
        $entity = $this->getObjectOf(Property::class, ['id' => $id]);
        static::assertEquals('testEditBuildingName', $entity->getName());
    }

    public function testLeasingsDateFilter()
    {
        $date = new \DateTime('+20 day');

        $result = $this->getList(null, ['leasings_date' => ['from' => $date->format('Y.m'), 'to' => $date->modify('+3 month')->format('Y.m')]]);

        static::assertListResult($result, 1, 1, 'name', 'First capital');
    }

    protected function getEntityName()
    {
        return Property::class;
    }
}