<?php

namespace AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadTemplateData;
use AppBundle\DataFixtures\Test\LoadPaymentData;
use AppBundle\Entity\Leasing;
use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentType;

/**
 * Class ContactControllerTest
 *
 * @package AppBundle\Controller
 */
class PaymentControllerTest extends AbstractControllerTest
{
    protected $url = '/api/payments';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadPaymentData::class,
            LoadTemplateData::class
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 2, 2, 'amount', 14830);
    }

    public function testCreate()
    {
        /** @var Leasing $leasing */
        $leasing = $this->getObjectOf(Leasing::class, ['notes' => 'test leasing']);
        /** @var PaymentType $type */
        $type = $this->getObjectOf(PaymentType::class, ['name' => 'rent']);

        $this->createItem([
            'amount' => 165460,
            'leasing' => $leasing->getId(),
            'type' => $type->getId(),
            'transactionType' => Payment::TRANSACTION_CREDIT,
            'dueDate' => (clone $leasing->getCurrentPayDay())->modify('+2 month')->format('d.m.Y'),
            'paymentMethod' => Payment::METHOD_CARD
        ]);

        /** @var Payment $entity */
        $entity = $this->getObjectOf(Payment::class, ['amount' => 165460]);
        static::assertEquals(165460, $entity->getAmount());
    }


    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['amount' => 33333], $id);

        /** @var Payment $entity */
        $entity = $this->getObjectOf(Payment::class, ['id' => $id]);
        static::assertEquals(33333, $entity->getAmount());
    }

    protected function getEntityName()
    {
        return Payment::class;
    }
}