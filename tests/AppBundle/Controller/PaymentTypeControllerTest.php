<?php
namespace AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadAccessTokenData;
use AppBundle\DataFixtures\ORM\LoadPaymentTypeData;
use AppBundle\Entity\PaymentType;

/**
 * Class PaymentTypeControllerTest
 *
 * @package AppBundle\Controller
 */
class PaymentTypeControllerTest extends AbstractControllerTest
{
    protected $url = '/api/payment-types';

    protected function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            LoadAccessTokenData::class,
            LoadPaymentTypeData::class,
        ]);
    }

    public function testList()
    {
        $result = $this->getList();

        static::assertListResult($result, 3, 3, 'name', 'electricity');
    }

    public function testCreate()
    {
        $this->createItem([
            'name' => 'test payment type',
        ]);

        /** @var PaymentType $entity */
        $entity = $this->getObjectOf(PaymentType::class, ['name' => 'test payment type']);
        static::assertEquals('test payment type', $entity->getName());
    }

    public function testEdit()
    {
        $id = $this->getExistedObjectId();
        $this->updateItem(['name' => 'updated payment type'], $id);

        /** @var PaymentType $entity */
        $entity = $this->getObjectOf(PaymentType::class, ['id' => $id]);
        static::assertEquals('updated payment type', $entity->getName());
    }

    protected function getEntityName()
    {
        return PaymentType::class;
    }
}