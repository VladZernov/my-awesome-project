<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EmailTemplateParameter
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApiRepository")
 *
 */
class EmailTemplateParameter
{
    const CONTACT_FULL_NAME = '%contact_full_name%';
    const UNIT_NAME = '%unit_name%';
    const PROPERTY_ADDRESS = '%property_address%';
    const RENT_AMOUNT = '%rent_amount%';
    const DOOR_CODE = '%door_code%';
    const WIFI = '%wifi%';
    const CHECK_IN_DATE = '%check_in_date%';
    const RENT_DUE_DATE = '%rent_due_date%';
    const DEPARTURE_DATE = '%departure_date%';
    const OUTSTANDINGS = '%outstandings%';
    const MONTHLY_UTILITY_CREDIT = '%monthly_utility_credit%';
    const SECURITY_DEPOSIT_AMOUNT = '%security_deposit_amount%';

    const PAYMENT_MONTH = '%payment_month%';

    const BANK_NAME = '%bank_name%';
    const BANK_ACCOUNT_NAME = '%bank_account_name%';
    const BANK_ACCOUNT_NUMBER = '%bank_account_number%';
    const BANK_CODE = '%bank_code%';
    const BANK_BRANCH_CODE = '%bank_branch_code%';
    const SWIFT = '%swift%';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $label;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
