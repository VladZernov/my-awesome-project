<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Unit
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UnitRepository")
 * @UniqueEntity(
 *     fields={"property", "name"},
 *     errorPath="name",
 * )
 */
class Unit
{
    use CurrentLeasing;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @Groups("default")
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $modelPrice;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $targetPrice;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $doorCode;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $wifi;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $notes;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $grossSize;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $netSize;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $bedSize;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Viewing", mappedBy="unit", cascade={"remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $viewings;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Leasing", mappedBy="unit", cascade={"persist", "remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $leasings;

    /**
     * @var Property
     *
     * @ORM\ManyToOne(targetEntity="Property", inversedBy="units")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups("default")
     * @Reference
     */
    private $property;

    /**
     *
     * @ORM\OneToMany(targetEntity="InventoryItem", mappedBy="unit", orphanRemoval=true, cascade={"persist", "remove"})
     *
     * @Groups("default")
     * @Reference
     *
     */
    private $inventoryItems;

    /**
     *
     * @ORM\OneToMany(targetEntity="UnitRepair", mappedBy="unit", orphanRemoval=true, cascade={"persist", "remove"})
     *
     * @Groups("default")
     *
     */
    private $repairs;

    /**
     *
     * @ORM\ManyToMany(targetEntity="File", cascade={"remove"})
     * @ORM\JoinTable(name="unit_files",
     *      joinColumns={@ORM\JoinColumn(name="unit_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     *
     * @Groups("default")
     *
     */
    protected $files;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->viewings = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
        $this->repairs = new ArrayCollection();
        $this->leasings = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Unit
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set modelPrice.
     *
     * @param int|null $modelPrice
     *
     * @return Unit
     */
    public function setModelPrice($modelPrice = null)
    {
        $this->modelPrice = $modelPrice;

        return $this;
    }

    /**
     * Get modelPrice.
     *
     * @return int|null
     */
    public function getModelPrice()
    {
        return $this->modelPrice;
    }

    /**
     * Set targetPrice.
     *
     * @param int|null $targetPrice
     *
     * @return Unit
     */
    public function setTargetPrice($targetPrice = null)
    {
        $this->targetPrice = $targetPrice;

        return $this;
    }

    /**
     * Get targetPrice.
     *
     * @return int|null
     */
    public function getTargetPrice()
    {
        return $this->targetPrice;
    }

    /**
     * Set doorCode.
     *
     * @param string|null $doorCode
     *
     * @return Unit
     */
    public function setDoorCode($doorCode = null)
    {
        $this->doorCode = $doorCode;

        return $this;
    }

    /**
     * Get doorCode.
     *
     * @return string|null
     */
    public function getDoorCode()
    {
        return $this->doorCode;
    }

    /**
     * Set wifi.
     *
     * @param string|null $wifi
     *
     * @return Unit
     */
    public function setWifi($wifi = null)
    {
        $this->wifi = $wifi;

        return $this;
    }

    /**
     * Get wifi.
     *
     * @return string|null
     */
    public function getWifi()
    {
        return $this->wifi;
    }

    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return Unit
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set grossSize.
     *
     * @param int $grossSize
     *
     * @return Unit
     */
    public function setGrossSize($grossSize)
    {
        $this->grossSize = $grossSize;

        return $this;
    }

    /**
     * Get grossSize.
     *
     * @return int
     */
    public function getGrossSize()
    {
        return $this->grossSize;
    }

    /**
     * Set netSize.
     *
     * @param int $netSize
     *
     * @return Unit
     */
    public function setNetSize($netSize)
    {
        $this->netSize = $netSize;

        return $this;
    }

    /**
     * Get netSize.
     *
     * @return int
     */
    public function getNetSize()
    {
        return $this->netSize;
    }

    /**
     * Set bedSize.
     *
     * @param string|null $bedSize
     *
     * @return Unit
     */
    public function setBedSize($bedSize = null)
    {
        $this->bedSize = $bedSize;

        return $this;
    }

    /**
     * Get bedSize.
     *
     * @return string|null
     */
    public function getBedSize()
    {
        return $this->bedSize;
    }

    /**
     * Add viewing.
     *
     * @param \AppBundle\Entity\Viewing $viewing
     *
     * @return Unit
     */
    public function addViewing(Viewing $viewing)
    {
        $this->viewings[] = $viewing;

        return $this;
    }

    /**
     * Remove viewing.
     *
     * @param \AppBundle\Entity\Viewing $viewing
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeViewing(Viewing $viewing)
    {
        return $this->viewings->removeElement($viewing);
    }

    /**
     * Get viewings.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewings()
    {
        return $this->viewings;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty($property)
    {
        $this->property = $property;
    }

    /**
     * Add file
     *
     * @param File $file
     *
     * @return Unit
     */
    public function addFile(File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param File $file
     */
    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add inventory item.
     *
     * @param InventoryItem $inventoryItem
     * @return Unit
     *
     */
    public function addInventoryItem($inventoryItem)
    {
        if (!$this->inventoryItems->contains($inventoryItem)) {
            $this->inventoryItems->add($inventoryItem);

            $inventoryItem->setUnit($this);
        }

        return $this;
    }

    /**
     * Remove inventory item.
     *
     * @param InventoryItem $inventoryItem
     *
     * @return Unit
     */
    public function removeInventoryItem($inventoryItem)
    {
        if ($this->inventoryItems->contains($inventoryItem)) {
            $this->inventoryItems->removeElement($inventoryItem);

            $inventoryItem->setUnit(null);
        }

        return $this;
    }

    /**
     * Get inventory.
     *
     * @return ArrayCollection
     */
    public function getInventoryItems()
    {
        return $this->inventoryItems;
    }

    /**
     * Add repair.
     *
     * @param UnitRepair $repair
     * @return Unit
     *
     */
    public function addRepair($repair)
    {
        if (!$this->repairs->contains($repair)) {
            $this->repairs->add($repair);

            $repair->setUnit($this);
        }

        return $this;
    }

    /**
     * Remove repair.
     *
     * @param UnitRepair $repair
     * @return Unit
     *
     */
    public function removeRepair($repair)
    {
        if ($this->repairs->contains($repair)) {
            $this->repairs->removeElement($repair);

            $repair->setUnit(null);
        }

        return $this;
    }

    /**
     * Get repairs.
     *
     * @return ArrayCollection
     */
    public function getRepairs()
    {
        return $this->repairs;
    }


    /**
     * @return ArrayCollection
     */
    public function getLeasings()
    {
        return $this->leasings;
    }

    /**
     * @param \DateTime $month
     * @return bool
     */
    public function isGapPeriod(\DateTime $month)
    {

        $leasingStart = $leasingEnd = false;

        $monthStart = \DateTime::createFromFormat('Y.m.d', $month->format('Y.m.01'));
        $monthEnd = \DateTime::createFromFormat('Y.m.d', $month->format('Y.m.t'));

        /** @var Leasing $leasing */
        foreach ($this->getLeasings() as $leasing) {

            if ($leasing->getStartDate() >= $monthStart) {
                $leasingStart = true;
            }

            if ($leasing->getEndDate() <= $monthEnd) {
                $leasingEnd = true;
            }
        }

        return $leasingStart && $leasingEnd;

    }

    /**
     * @param \DateTime $month
     * @return bool
     */
    public function isExpiryPeriod(\DateTime $month)
    {
        $expr = Criteria::expr();
        $endPeriodCriteria = new Criteria();
        $endPeriodCriteria
            ->where($expr->andX($expr->gte('endDate', \DateTime::createFromFormat('Y.m.d', $month->format('Y.m.01'))),
                $expr->lte('endDate',  \DateTime::createFromFormat('Y.m.d', $month->format('Y.m.t')))
            ));

        return (bool) $this->getLeasings()->matching($endPeriodCriteria)->first();
    }
}

