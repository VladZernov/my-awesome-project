<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * EmailRecipient
 *
 * @ORM\Entity
 */
class EmailRecipient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var Email
     *
     * @ORM\ManyToOne(targetEntity="Email", inversedBy="recipients")
     */
    private $mail;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact")
     *
     * @Groups("default")
     * @Reference
     */
    private $contact;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $sesMessageId;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $status;

    /**
     * EmailRecipient constructor.
     * @param Contact|null $contact
     */
    public function __construct($contact = null)
    {
        $this->contact = $contact;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Email
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param Email $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param ArrayCollection $contact
     * @return $this
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getSesMessageId()
    {
        return $this->sesMessageId;
    }

    /**
     * @param null|string $sesMessageId
     */
    public function setSesMessageId($sesMessageId)
    {
        $this->sesMessageId = $sesMessageId;
    }

    /**
     * @return null|string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param null|string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
