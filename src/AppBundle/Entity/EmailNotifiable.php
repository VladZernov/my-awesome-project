<?php

namespace AppBundle\Entity;


interface EmailNotifiable
{
    /**
     * @return array
     */
    public function getEmailParameters();
}