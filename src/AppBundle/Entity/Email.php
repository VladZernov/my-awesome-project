<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * File
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApiRepository")
 */
class Email
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string Mail subject
     *
     * @ORM\Column(type="string")
     *
     * @Groups("default")
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="text")
     *
     * @Groups("default")
     */
    private $message;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="EmailRecipient", mappedBy="mail", cascade={"persist", "remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $recipients;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="File", cascade={"persist", "remove"})
     *
     * @Groups("default")
     */
    private $attachments;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $status;

    /**
     * Email constructor.
     */
    public function __construct()
    {
        $this->attachments = new ArrayCollection();
        $this->recipients = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param ArrayCollection $attachments
     * @return $this
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * @param File $attachment
     *
     * @return Email
     */
    public function addAttachment($attachment)
    {
        if (!$this->attachments->contains($attachment)) {

            $this->attachments->add($attachment);
        }

        return $this;
    }

    /**
     * @param File $attachment
     *
     * @return Email
     */
    public function removeAttachment($attachment)
    {
        if ($this->attachments->contains($attachment)) {

            $this->attachments->removeElement($attachment);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @return array
     */
    public function getRecipientsEmails()
    {
        $recipientsEmails = [];

        /** @var EmailRecipient $recipient */
        foreach($this->getRecipients() as $recipient) {

            if (!$recipient->getContact()->isEmailCompromised()) {
                $recipientsEmails[] = $recipient->getContact()->getEmail();
            }
        }

        return $recipientsEmails;
    }

    /**
     * @param EmailRecipient $recipient
     *
     * @return Email
     */
    public function addRecipient($recipient)
    {
        if (!$this->recipients->contains($recipient)) {
            $this->recipients->add($recipient);
        }

        return $this;
    }

    /**
     * @param EmailRecipient $recipient
     *
     * @return Email
     */
    public function removeRecipient($recipient)
    {
        if ($this->recipients->contains($recipient)) {
            $this->recipients->removeElement($recipient);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
