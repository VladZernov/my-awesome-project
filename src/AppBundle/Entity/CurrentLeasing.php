<?php

namespace AppBundle\Entity;

use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait CurrentLeasing
 *
 * @package AppBundle\Entity
 */
trait CurrentLeasing
{

    /**
     * @var Leasing
     *
     * @ORM\ManyToOne(targetEntity="Leasing", cascade={"remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     *
     * @Groups("default")
     * @Reference
     */
    private $currentLeasing;

    /**
     * @return Leasing
     */
    public function getCurrentLeasing()
    {
        return $this->currentLeasing;
    }

    /**
     * @param Leasing $currentLeasing
     */
    public function setCurrentLeasing($currentLeasing)
    {
        $this->currentLeasing = $currentLeasing;
    }
}