<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Payment
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentRepository")
 */
class Payment implements EmailNotifiable
{
    const TRANSACTION_DEBIT = 'debit';
    const TRANSACTION_CREDIT = 'credit';

    const METHOD_CARD = 'Credit card';
    const METHOD_CHECK = 'Bank check';

    const LATE_PERIOD = 5;
    const PENALTY_PERIOD = 5;
    const REMIND_PERIOD = 10;
    const RENT_INTERVAL = 15;
    const DAYS_BEFORE = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(
     *      choices = {
     *          Payment::TRANSACTION_DEBIT,
     *          Payment::TRANSACTION_CREDIT
     *      }
     * )
     *
     * @Groups("default")
     */
    private $transactionType;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Groups("default")
     */
    private $amount;

    /**
     * @var Leasing
     *
     * @ORM\ManyToOne(targetEntity="Leasing", inversedBy="payments")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups("default")
     * @Reference
     */
    private $leasing;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="payments")
     *
     * @Groups("default")
     *
     * @Reference
     */
    private $contact;

    /**
     * @var PaymentType
     *
     * @ORM\ManyToOne(targetEntity="PaymentType")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups("default")
     */
    private $type;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Expression(
     *     "value > this.getAllowedPaymentDate()",
     *     message="Due date must be at least Payment::DAYS_BEFORE after today."
     * )
     *
     * @Groups("default")
     */
    private $dueDate;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("default")
     */
    private $paid;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups("default")
     */
    private $paidDate;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("default")
     */
    private $outdated;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Choice(
     *      choices = {
     *          Payment::METHOD_CARD,
     *          Payment::METHOD_CHECK
     *      }
     * )
     *
     * @Groups("default")
     */
    private $paymentMethod;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups("default")
     */
    private $comment;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     * @return $this
     */
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Leasing
     */
    public function getLeasing()
    {
        return $this->leasing;
    }

    /**
     * @param Leasing $leasing
     * @return $this
     */
    public function setLeasing($leasing)
    {
        $this->leasing = $leasing;
        $this->setContact($leasing->getContact());

        return $this;
    }

    /**
     * @return PaymentType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param PaymentType $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     * @return $this
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return $this->paid;
    }

    /**
     * @param bool $paid
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    /**
     * @return \DateTime
     */
    public function getPaidDate()
    {
        return $this->paidDate;
    }

    /**
     * @param \DateTime $paidDate
     */
    public function setPaidDate($paidDate)
    {
        $this->paidDate = $paidDate;
    }

    /**
     * @return bool
     */
    public function isOutdated()
    {
        return $this->outdated;
    }

    /**
     * @param bool $outdated
     */
    public function setOutdated($outdated)
    {
        $this->outdated = $outdated;
    }

    /**
     * @return array
     */
    public function getEmailParameters()
    {
        return array_merge($this->leasing->getEmailParameters(), [
            '%month%' => $this->getDueDate()->format('F'),
        ]);
    }

    /**
     * @return \DateTime
     */
    public function getAllowedPaymentDate()
    {
        $days = Payment::DAYS_BEFORE;

        return new \DateTime("+ $days day");
    }
}
