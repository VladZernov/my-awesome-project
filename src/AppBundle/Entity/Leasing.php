<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use AppBundle\Validator as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Requestum\ApiBundle\Rest\Metadata\Reference;

/**
 * Leasing
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeasingRepository")
 * @AppAssert\LeasingAllowed()
 * @ORM\EntityListeners({"AppBundle\EventListener\LeasingListener"})
 */
class Leasing implements EmailNotifiable
{
    const RENEW_PERIOD = 45;
    const CHECKIN_PERIOD = 1;
    const CHECKOUT_PERIOD = 21;
    const DEFAULT_PAY_DAY = 20;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Groups("default")
     */
    private $price;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="leasings", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Groups("default")
     * @Reference
     */
    private $contact;

    /**
     * @var Unit
     *
     * @ORM\ManyToOne(targetEntity="Unit", inversedBy="leasings", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups("default")
     * @Reference
     */
    private $unit;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Payment", mappedBy="leasing", cascade={"remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $payments;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Expression(
     *     "value < this.getEndDate()",
     *     message="Start date must be earlier then end date"
     * )
     * @Assert\Expression(
     *     "value > this.getCurrentDate()",
     *     message="Start date must be in the future",
     *     groups={"creation"}
     * )
     *
     * @Groups("default")
     */
    private $startDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Expression(
     *     "value > this.getStartDate()",
     *     message="End date must be later then start date"
     * )
     *
     * @Groups("default")
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 31,
     * )
     *
     * @Groups("default")
     */
    private $payDay = self::DEFAULT_PAY_DAY;

    /**
     * @var LeasingType
     *
     * @ORM\ManyToOne(targetEntity="LeasingType")
     *
     * @Groups("default")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups("default")
     */
    private $credits;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups("default")
     */
    private $paymentInfo;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups("default")
     */
    private $notes;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $monthlyUtilityCredit;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $securityDepositAmount;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Contact")
     *
     * @Groups("default")
     * @Reference
     */
    private $guests;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     *
     * @Groups("default")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups("default")
     */
    protected $updateAt;

    /**
     * Leasing constructor.
     */
    public function __construct()
    {
        $this->payments = new ArrayCollection();
        $this->guests = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return Leasing
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set contact.
     *
     * @param Contact|null $contact
     *
     * @return Leasing
     */
    public function setContact($contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set unit.
     *
     * @param Unit|null $unit
     *
     * @return Leasing
     */
    public function setUnit($unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit.
     *
     * @return Unit|null
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     *
     * @return Leasing
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     *
     * @return Leasing
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return LeasingType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param LeasingType $type
     *
     * @return Leasing
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param ArrayCollection $payments
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;
    }

    /**
     * Sets createdAt.
     *
     * @param  \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTime $updateAt
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;
    }

    /**
     * @return string
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param string $credits
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
    }

    /**
     * @return string
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }

    /**
     * @param string $paymentInfo
     */
    public function setPaymentInfo($paymentInfo)
    {
        $this->paymentInfo = $paymentInfo;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return $this
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return int
     */
    public function getMonthlyUtilityCredit()
    {
        return $this->monthlyUtilityCredit;
    }

    /**
     * @param int $monthlyUtilityCredit
     * @return $this
     */
    public function setMonthlyUtilityCredit($monthlyUtilityCredit)
    {
        $this->monthlyUtilityCredit = $monthlyUtilityCredit;

        return $this;
    }

    /**
     * @return int
     */
    public function getSecurityDepositAmount()
    {
        return $this->securityDepositAmount;
    }

    /**
     * @param int $securityDepositAmount
     * @return $this
     */
    public function setSecurityDepositAmount($securityDepositAmount)
    {
        $this->securityDepositAmount = $securityDepositAmount;

        return $this;
    }

    /**
     *
     * @param ArrayCollection|null $guests
     *
     * @return $this
     */
    public function setGuests($guests = null)
    {
        $this->guests = $guests;

        return $this;
    }

    /**
     *
     * @return ArrayCollection|null
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * @param Contact $guest
     *
     * @return $this
     */
    public function addGuest($guest)
    {
        if (!$this->guests->contains($guest)) {
            $this->guests->add($guest);
        }

        return $this;
    }

    /**
     * @param Contact $guest
     *
     * @return $this
     */
    public function removeGuest($guest)
    {
        if ($this->guests->contains($guest)) {
            $this->guests->removeElement($guest);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getPayDay()
    {
        return $this->payDay;
    }

    /**
     * @param int $payDay
     * @return $this
     */
    public function setPayDay($payDay)
    {
        $this->payDay = !empty($payDay) ? $payDay:self::DEFAULT_PAY_DAY;

        return $this;
    }

    /**
     * @return array
     */
    public function getEmailParameters()
    {
        return [
            EmailTemplateParameter::CONTACT_FULL_NAME => $this->getContact()->getFullName(),
            EmailTemplateParameter::UNIT_NAME => $this->getProperty()->getName().', '.$this->getUnit()->getName(),
            EmailTemplateParameter::PROPERTY_ADDRESS => $this->getProperty()->getFullAddress(),
            EmailTemplateParameter::RENT_AMOUNT => $this->getPrice(),
            EmailTemplateParameter::DOOR_CODE => $this->getUnit()->getDoorCode(),
            EmailTemplateParameter::WIFI => $this->getUnit()->getWifi(),
            EmailTemplateParameter::CHECK_IN_DATE => $this->getStartDate()->format('d.m.Y'),
            EmailTemplateParameter::RENT_DUE_DATE => $this->getCurrentPayDay()->format('d.m.Y'),
            EmailTemplateParameter::DEPARTURE_DATE => $this->getEndDate()->format('d.m.Y'),
            EmailTemplateParameter::BANK_NAME => $this->getBankInfo()->getName(),
            EmailTemplateParameter::BANK_ACCOUNT_NAME => $this->getBankInfo()->getAccountName(),
            EmailTemplateParameter::BANK_ACCOUNT_NUMBER => $this->getBankInfo()->getAccountNumber(),
            EmailTemplateParameter::BANK_CODE => $this->getBankInfo()->getCode(),
            EmailTemplateParameter::BANK_BRANCH_CODE => $this->getBankInfo()->getBranchCode(),
            EmailTemplateParameter::SWIFT => $this->getBankInfo()->getSwift(),
            EmailTemplateParameter::OUTSTANDINGS => $this->getFormattedOutstandings(),
            EmailTemplateParameter::MONTHLY_UTILITY_CREDIT => $this->getMonthlyUtilityCredit(),
            EmailTemplateParameter::SECURITY_DEPOSIT_AMOUNT => $this->getSecurityDepositAmount(),
        ];
    }

    /**
     * @return \DateTime
     */
    public function getCurrentDate()
    {
        return new \DateTime();
    }

    /**
     * @return bool
     */
    public function isLastMonth()
    {
        $rentInterval = Payment::RENT_INTERVAL;
        $payMonth = $this->getCurrentDate()->modify("+$rentInterval day")->format('m.Y');

        return $this->getEndDate()->format('m.Y') === $payMonth;
    }

    /**
     * @return bool
     */
    public function isCurrent()
    {
        return $this->getStartDate() <= $this->getCurrentDate() && $this->getEndDate() >= $this->getCurrentDate();
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->getUnit()->getProperty();
    }

    /**
     * @return BankInfo
     */
    public function getBankInfo()
    {
        return $this->getProperty()->getBankInfo();
    }

    /**
     * @return array
     */
    public function getOutstandings()
    {
        $outstandings = [];

        /** @var Payment $payment */
        foreach ($this->payments as $payment) {
            if ($payment->isOutdated()) {
                $outstandings[$payment->getType()->getName()] += $payment->getAmount();
            }
        }

        return $outstandings;
    }

    /**
     * @return string
     */
    public function getFormattedOutstandings()
    {
        $outstandings = $this->getOutstandings();
        $outstandingsStrings = array_map(
            function ($k, $v) {
                return "$k - $v";
            },
            array_keys($outstandings),
            $outstandings
        );

        return implode('<br/>', $outstandingsStrings);
    }

    /**
     * @return \DateTime
     */
    public function getCurrentPayDay()
    {
        $currentPayDay = \DateTime::createFromFormat('j', $this->payDay);

        if ($this->getCurrentDate()->format('d') > $this->payDay) {

            $currentPayDay->modify('+1 month');
        }

        return $currentPayDay;
    }
}
