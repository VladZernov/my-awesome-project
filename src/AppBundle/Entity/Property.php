<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Property
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PropertyRepository")
 */
class Property
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $city;

    /**
     * @var ArrayCollection $units
     *
     * @ORM\OneToMany(targetEntity="Unit", mappedBy="property", cascade={"remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $units;

    /**
     * @var BankInfo
     *
     * @ORM\ManyToOne(targetEntity="BankInfo", inversedBy="properties", cascade={"persist"})
     * @ORM\JoinColumn(name="bank_info_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     *
     * @Groups("default")
     * @Reference
     */
    private $bankInfo;

    /**
     * ContactGroup constructor.
     */
    public function __construct()
    {
        $this->units = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Property
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param ArrayCollection $units
     *
     * @return Property
     */
    public function setUnits($units)
    {
        $this->units = $units;

        return $this;
    }

    /**
     * @param Unit $unit
     *
     * @return Property
     */
    public function addUnit($unit)
    {
        if (!$this->units->contains($unit)) {
            $this->units->add($unit);

            $unit->setProperty($this);
        }

        return $this;
    }

    /**
     * @param Unit $unit
     *
     * @return Property
     */
    public function removeUnit($unit)
    {
        if ($this->units->contains($unit)) {
            $this->units->removeElement($unit);

            $unit->setProperty(null);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     * @return $this
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return BankInfo
     */
    public function getBankInfo()
    {
        return $this->bankInfo;
    }

    /**
     * @param BankInfo $bankInfo
     * @return $this
     */
    public function setBankInfo($bankInfo)
    {
        $this->bankInfo = $bankInfo;

        return $this;
    }

    public function getFullAddress()
    {
        return $this->city.", ".$this->address.", ".$this->zipcode;
    }
}
