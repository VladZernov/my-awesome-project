<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Accounting
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApiRepository")
 */
class Accounting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var ExpenseType
     *
     * @ORM\ManyToOne(targetEntity="ExpenseType")
     *
     * @Groups("default")
     */
    private $expenseType;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Groups("default")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $notes;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups("default")
     */
    private $date;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ExpenseType
     */
    public function getExpenseType()
    {
        return $this->expenseType;
    }

    /**
     * @param ExpenseType $expenseType
     */
    public function setExpenseType($expenseType)
    {
        $this->expenseType = $expenseType;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}
