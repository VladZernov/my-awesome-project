<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 */
class Contact
{
    use CurrentLeasing;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank
     * @Assert\Email()
     *
     * @Groups("default")
     */
    private $email;


    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("default")
     */
    private $emailCompromised;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank
     *
     * @Groups("default")
     */
    private $phone;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $budget;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $employer;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $profession;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(
     *      choices = {
     *          "male",
     *          "female"
     *      }
     * )
     *
     * @Groups("default")
     */
    private $gender;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $age;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups("default")
     */
    private $salary;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="File", cascade={"persist", "remove"})
     *
     * @Groups("default")
     */
    private $documents;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContactGroup", inversedBy="contacts")
     *
     * @Groups("default")
     * @Reference
     */
    private $groups;

    /**
     * @var ContactCampaignSource
     *
     * @ORM\ManyToOne(targetEntity="ContactCampaignSource", inversedBy="contacts")
     *
     * @Groups("default")
     */
    private $campaignSource;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("default")
     */
    private $active;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Viewing", mappedBy="contact")
     *
     * @Groups("default")
     * @Reference
     */
    private $viewings;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Leasing", mappedBy="contact", cascade={"persist", "remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $leasings;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Payment", mappedBy="contact")
     *
     * @Groups("default")
     * @Reference
     */
    private $payments;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true, unique=false)
     *
     * @Groups("default")
     */
    private $notes;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * Contact constructor.
     */
    public function __construct()
    {
        $this->viewings = new ArrayCollection();
        $this->leasings = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->emailCompromised = false;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return Contact
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return Contact
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getFullName()
    {
        return $this->firstName.' '.$this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set number.
     *
     * @param string|null $number
     *
     * @return Contact
     */
    public function setPhone($number = null)
    {
        $this->phone = $number;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set budget.
     *
     * @param int|null $budget
     *
     * @return Contact
     */
    public function setBudget($budget = null)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget.
     *
     * @return int|null
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set employer.
     *
     * @param string|null $employer
     *
     * @return Contact
     */
    public function setEmployer($employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer.
     *
     * @return string|null
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Set profession.
     *
     * @param string|null $profession
     *
     * @return Contact
     */
    public function setProfession($profession = null)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession.
     *
     * @return string|null
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @return mixed
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param File $document
     *
     * @return Contact
     */
    public function addDocument($document)
    {
        if (!$this->documents->contains($document)) {

            $this->documents->add($document);
        }

        return $this;
    }

    /**
     * @param File $document
     *
     * @return Contact
     */
    public function removeDocument($document)
    {
        if ($this->documents->contains($document)) {

            $this->documents->removeElement($document);
        }

        return $this;
    }

    /**
     * Set groups.
     *
     * @param ArrayCollection|null $groups
     *
     * @return Contact
     */
    public function setGroups($groups = null)
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * Get groups.
     *
     * @return ArrayCollection|null
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param ContactGroup $group
     *
     * @return Contact
     */
    public function addGroup($group)
    {
        if (!$this->groups->contains($group)) {
            $this->groups->add($group);

            $group->addContact($this);
        }

        return $this;
    }

    /**
     * @param ContactGroup $group
     *
     * @return Contact
     */
    public function removeGroup($group)
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);

            $group->removeContact($this);
        }

        return $this;
    }

    /**
     * Set campaign source.
     *
     * @param ContactCampaignSource|null $campaignSource
     *
     * @return Contact
     */
    public function setCampaignSource(ContactCampaignSource $campaignSource = null)
    {
        $this->campaignSource = $campaignSource;

        return $this;
    }

    /**
     * Get campaign source.
     *
     * @return ContactCampaignSource|null
     */
    public function getCampaignSource()
    {
        return $this->campaignSource;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return ArrayCollection
     */
    public function getViewings()
    {
        return $this->viewings;
    }

    /**
     * @param mixed $viewings
     */
    public function setViewings($viewings)
    {
        $this->viewings = $viewings;
    }

    /**
     * Sets createdAt.
     *
     * @param  \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return ArrayCollection
     */
    public function getLeasings()
    {
        return $this->leasings;
    }

    /**
     * @return null|string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param null|string $gender
     *
     * @return Contact
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int|null $age
     *
     * @return Contact
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return int
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param int $salary
     *
     * @return Contact
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }


    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return Contact
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @return ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @return bool
     */
    public function isEmailCompromised()
    {
        return $this->emailCompromised;
    }

    /**
     * @param bool $emailCompromised
     */
    public function setEmailCompromised($emailCompromised)
    {
        $this->emailCompromised = $emailCompromised;
    }
}
