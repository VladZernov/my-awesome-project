<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApiRepository")
 * @ORM\EntityListeners({"AppBundle\EventListener\UserListener"})
 * @UniqueEntity("email")
 */
class User implements UserInterface
{

    const ROLE_ADMIN = 'admin';
    const ROLE_VIEWER = 'viewer';
    const ROLE_EDITOR = 'editor';
    const ROLE_USER = 'user';

    use TimestampableEntity;

    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    protected $externalId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    protected $externalService;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    protected $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    protected $email;

    /**
     * @var string Encrypted password.
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var string Encrypted password.
     *
     * @Assert\NotBlank(groups={"create"})
     */
    protected $plainPassword;

    /**
     * @var string User role
     *
     * @ORM\Column(type="string")
     * @Assert\Choice(
     *      choices = {
     *          User::ROLE_ADMIN,
     *          User::ROLE_VIEWER,
     *          User::ROLE_EDITOR,
     *          User::ROLE_USER
     *      }
     * )
     *
     * @Groups("default")
     */
    protected $role = User::ROLE_USER;

    /**
     * @var int
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("default")
     */
    protected $enabled = true;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return $this
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPlainPassword($password)
    {
        $this->setPassword(null);

        $this->plainPassword = $password;

        return $this;
    }

    /**
     * @return bool|string
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $boolean
     * @return $this
     */
    public function setEnabled($boolean)
    {
        $this->enabled = $boolean;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalService()
    {
        return $this->externalService;
    }

    /**
     * @param string $externalService
     */
    public function setExternalService($externalService)
    {
        $this->externalService = $externalService;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * @return array
     */
    public static function getRolesChoices()
    {
        return [
            self::ROLE_ADMIN,
            self::ROLE_VIEWER,
            self::ROLE_EDITOR,
            self::ROLE_USER,
        ];
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
