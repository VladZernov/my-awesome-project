<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;
use AppBundle\Validator as AppAssert;

/**
 * Viewing
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ViewingRepository")
 * @ORM\EntityListeners({"AppBundle\EventListener\ViewingListener"})
 * @AppAssert\ViewingAllowed()
 */
class Viewing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Groups("default")
     */
    private $price;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     *
     * @Groups("default")
     */
    private $notes;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="viewings")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups("default")
     * @Reference
     */
    private $contact;

    /**
     * @var Unit
     *
     * @ORM\ManyToOne(targetEntity="Unit", inversedBy="viewings")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups("default")
     * @Reference
     */
    private $unit;

    /**
     * @var ViewingStage
     *
     * @ORM\ManyToOne(targetEntity="ViewingStage", inversedBy="viewings")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups("default")
     * @Reference
     */
    private $stage;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups("default")
     */
    private $date;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return Viewing
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return Viewing
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set contact.
     *
     * @param Contact|null $contact
     *
     * @return Viewing
     */
    public function setContact($contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set unit.
     *
     * @param Unit|null $unit
     *
     * @return Viewing
     */
    public function setUnit($unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit.
     *
     * @return Unit|null
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set stage.
     *
     * @param ViewingStage|null $stage
     *
     * @return Viewing
     */
    public function setStage($stage = null)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage.
     *
     * @return ViewingStage|null
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Sets createdAt.
     *
     * @param  \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets updatedAt.
     *
     * @param  \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
