<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ContactGroup
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApiRepository")
 */
class ContactGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=false)
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $name;

    /**
     * @var ArrayCollection $contacts
     *
     * @ORM\ManyToMany(targetEntity="Contact", mappedBy="groups")
     *
     * @Groups("default")
     * @Reference
     */
    private $contacts;

    /**
     * ContactGroup constructor.
     */
    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ContactGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param ArrayCollection $contacts
     *
     * @return ContactGroup
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @param Contact $contact
     *
     * @return ContactGroup
     */
    public function addContact($contact)
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);

            $contact->addGroup($this);
        }

        return $this;
    }

    /**
     * @param Contact $contact
     *
     * @return ContactGroup
     */
    public function removeContact($contact)
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);

            $contact->removeGroup($this);
        }

        return $this;
    }
}
