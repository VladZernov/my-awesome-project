<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ViewingStage
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ViewingStageRepository")
 * @UniqueEntity("name")
 * @ORM\EntityListeners({"AppBundle\EventListener\ViewingStageListener"})
 */
class ViewingStage
{

    const START_STAGE = 'start_stage';
    const MIDDLE_STAGE = 'middle_stage';
    const FINISH_STAGE = 'finish_stage';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $label;

    /**
     * @var int
     *
     * @ORM\Column(name="view_order", type="integer")
     *
     * @Groups("default")
     */
    private $order;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Choice(
     *      choices = {
     *          ViewingStage::START_STAGE,
     *          ViewingStage::MIDDLE_STAGE,
     *          ViewingStage::FINISH_STAGE
     *      }
     * )
     *
     * @Groups("default")
     */
    private $flowFlag;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Viewing", mappedBy="stage", cascade={"remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $viewings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->viewings = new ArrayCollection();
        $this->flowFlag = self::MIDDLE_STAGE;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ViewingStage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add viewing.
     *
     * @param \AppBundle\Entity\Viewing $viewing
     *
     * @return ViewingStage
     */
    public function addViewing(Viewing $viewing)
    {
        $this->viewings[] = $viewing;

        return $this;
    }

    /**
     * Remove viewing.
     *
     * @param Viewing $viewing
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeViewing(Viewing $viewing)
    {
        return $this->viewings->removeElement($viewing);
    }

    /**
     * Get viewings.
     *
     * @return ArrayCollection
     */
    public function getViewings()
    {
        return $this->viewings;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return $this
     */
    public function incOrder()
    {
        $this->order++;

        return $this;
    }

    /**
     * @return $this
     */
    public function decOrder()
    {
        $this->order--;

        return $this;
    }

    /**
     * @return bool
     */
    public function getFlowFlag()
    {
        return $this->flowFlag;
    }

    /**
     * @param string $flowFlag
     *
     * @return $this
     */
    public function setFlowFlag($flowFlag)
    {
        $this->flowFlag = !empty($flowFlag) ? $flowFlag:self::MIDDLE_STAGE;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStart()
    {
        return $this->getFlowFlag() == self::START_STAGE;
    }

    /**
     * @return bool
     */
    public function isMiddle()
    {
        return $this->getFlowFlag() == self::MIDDLE_STAGE;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->getFlowFlag() == self::FINISH_STAGE;
    }
}
