<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Requestum\ApiBundle\Rest\Metadata\Reference;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BankInfo
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApiRepository")
 *
 */
class BankInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $accountName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $accountNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $branchCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $swift;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Property", mappedBy="bankInfo", cascade={"persist", "remove"})
     *
     * @Groups("default")
     * @Reference
     */
    private $properties;

    /**
     * BankInfo constructor.
     */
    public function __construct()
    {
        $this->properties = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * @param string $accountName
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getBranchCode()
    {
        return $this->branchCode;
    }

    /**
     * @param string $branchCode
     */
    public function setBranchCode($branchCode)
    {
        $this->branchCode = $branchCode;
    }

    /**
     * @return string
     */
    public function getSwift()
    {
        return $this->swift;
    }

    /**
     * @param string $swift
     */
    public function setSwift($swift)
    {
        $this->swift = $swift;
    }

    /**
     * @return ArrayCollection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param Property $property
     *
     * @return BankInfo
     */
    public function addProperty($property)
    {
        if (!$this->properties->contains($property)) {
            $this->properties->add($property);

            $property->setBankInfo($this);
        }

        return $this;
    }

    /**
     * @param Property $property
     *
     * @return BankInfo
     */
    public function removeProperty($property)
    {
        if ($this->properties->contains($property)) {
            $this->properties->removeElement($property);

            $property->setBankInfo(null);
        }

        return $this;
    }




}
