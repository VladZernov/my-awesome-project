<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator as AppAssert;

/**
 * EmailTemplate
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmailTemplateRepository")
 * @ORM\EntityListeners({"AppBundle\EventListener\EmailTemplateListener"})
 * @AppAssert\TemplateActive()
 */
class EmailTemplate
{
    const CHECKIN_TYPE = 'checkin'; // on leasing creation
    const CHECKIN_REMIND_TYPE = 'checkin_remind'; // day before checkin
    const REMIND_TYPE = 'remind'; // 10 days before rent date
    const INVOICE_TYPE = 'invoice'; // in day of rent
    const LATE_TYPE = 'late'; // 5 days after rent date

    const RENEW_TYPE = 'renew'; // ?(45) days before checkout if MTM
    const CHECKOUT_TYPE = 'checkout'; // ?(21) days before checkout

    const REGISTRATION_TYPE = 'registered'; // after user registered

    const RECEIPT_TYPE = 'receipt'; // after payment paid

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups("default")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Choice(
     *      choices = {
     *          EmailTemplate::REMIND_TYPE,
     *          EmailTemplate::LATE_TYPE,
     *          EmailTemplate::REGISTRATION_TYPE,
     *          EmailTemplate::RENEW_TYPE,
     *          EmailTemplate::CHECKIN_TYPE,
     *          EmailTemplate::CHECKIN_REMIND_TYPE,
     *          EmailTemplate::CHECKOUT_TYPE,
     *          EmailTemplate::RECEIPT_TYPE,
     *          EmailTemplate::INVOICE_TYPE
     *      }
     * )
     *
     * @Groups("default")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups("default")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     *
     * @Groups("default")
     */
    private $subject;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("default")
     */
    private $active;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="File", cascade={"persist", "remove"})
     *
     * @Groups("default")
     */
    private $attachments;

    /**
     * EmailTemplate constructor.
     */
    public function __construct()
    {
        $this->attachments = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return EmailTemplate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param File $attachment
     *
     * @return EmailTemplate
     */
    public function addAttachment($attachment)
    {
        if (!$this->attachments->contains($attachment)) {

            $this->attachments->add($attachment);
        }

        return $this;
    }

    /**
     * @param File $attachment
     *
     * @return EmailTemplate
     */
    public function removeAttachment($attachment)
    {
        if ($this->attachments->contains($attachment)) {

            $this->attachments->removeElement($attachment);
        }

        return $this;
    }

    public static function getTypesChoices()
    {
        return [
            EmailTemplate::REMIND_TYPE,
            EmailTemplate::LATE_TYPE,
            EmailTemplate::REGISTRATION_TYPE,
            EmailTemplate::RENEW_TYPE,
            EmailTemplate::CHECKIN_TYPE,
            EmailTemplate::CHECKIN_REMIND_TYPE,
            EmailTemplate::CHECKOUT_TYPE,
            EmailTemplate::RECEIPT_TYPE,
            EmailTemplate::INVOICE_TYPE,
        ];
    }

}
