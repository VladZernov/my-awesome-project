<?php

namespace AppBundle\Form;

use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\LeasingType as LeasingTypeEntity;

/**
 * Class LeasingTypeForm
 */
class LeasingTypeForm extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LeasingTypeEntity::class,
            'allow_extra_fields' => true,
        ]);
    }
}