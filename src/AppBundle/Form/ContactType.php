<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactGroup;
use AppBundle\Entity\ContactCampaignSource;
use AppBundle\Entity\File;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactType
 */
class ContactType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('email', EmailType::class)
            ->add('phone')
            ->add('budget')
            ->add('employer')
            ->add('profession')
            ->add('gender')
            ->add('age')
            ->add('salary')
            ->add('notes')
            ->add('documents', EntityType::class, [
                'class' => File::class,
                'label' => 'File id',
                'multiple' => true,
                'by_reference' => false,
            ])
            ->add('groups', EntityType::class, [
                'class' => ContactGroup::class,
                'label' => 'Group id',
                'multiple' => true,
                'by_reference' => false,
            ])
            ->add('campaignSource', EntityType::class, [
                'class' => ContactCampaignSource::class,
                'label' => 'Campaign Source id',
            ])
            ->add('active', ChoiceType::class, [
                'choices' => [true, false],
                'description' => 'Makes contact show up in payments searches, leasing, etc.)',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'allow_extra_fields' => true,
        ]);
    }
}