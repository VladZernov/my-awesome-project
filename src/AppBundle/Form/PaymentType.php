<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Leasing;
use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentType as PaymentTypeEntity;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaymentType
 */
class PaymentType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('transactionType')
            ->add('amount')
            ->add('leasing', EntityType::class, [
                'class' => Leasing::class,
                'label' => 'Leasing id',
            ])
            ->add('type', EntityType::class, [
                'class' => PaymentTypeEntity::class,
                'label' => 'Payment type id',
            ])
            ->add('dueDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
            ])
            ->add('contact', EntityType::class, [
                'class' => Contact::class,
                'label' => 'Contact id',
            ])
            ->add('paymentMethod')
            ->add('comment');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Payment::class,
            'allow_extra_fields' => true,
        ]);
    }
}