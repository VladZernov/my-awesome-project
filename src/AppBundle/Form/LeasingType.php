<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Leasing;
use AppBundle\Entity\Unit;
use AppBundle\Entity\LeasingType as LeasingTypeEntity;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LeasingType
 */
class LeasingType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price')
            ->add('contact', EntityType::class, [
                'class' => Contact::class,
                'label' => 'Contact id',
            ])
            ->add('unit', EntityType::class, [
                'class' => Unit::class,
                'label' => 'Unit id',
            ])
            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
            ])
            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
            ])
            ->add('payDay')
            ->add('type', EntityType::class, [
                'class' => LeasingTypeEntity::class,
                'label' => 'Leasing Type id',
            ])
            ->add('credits')
            ->add('paymentInfo')
            ->add('notes')
            ->add('monthlyUtilityCredit')
            ->add('securityDepositAmount')
            ->add('guests', EntityType::class, [
                'class' => Contact::class,
                'label' => 'Guest id',
                'multiple' => true,
                'by_reference' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Leasing::class,
            'allow_extra_fields' => true,
        ]);
    }
}