<?php

namespace AppBundle\Form;

use AppBundle\Entity\BankInfo;
use AppBundle\Entity\Property;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactType
 */
class BankInfoType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('accountName')
            ->add('accountNumber', EmailType::class)
            ->add('branchCode')
            ->add('code')
            ->add('swift')
            ->add('properties', EntityType::class, [
                'class' => Property::class,
                'label' => 'Property id',
                'multiple' => true,
                'by_reference' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BankInfo::class,
            'allow_extra_fields' => true,
        ]);
    }
}