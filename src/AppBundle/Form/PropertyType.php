<?php

namespace AppBundle\Form;

use AppBundle\Entity\BankInfo;
use AppBundle\Entity\Property;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PropertyType
 */
class PropertyType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('city')
            ->add('address')
            ->add('zipcode')
            ->add('bankInfo', EntityType::class, [
                'class' => BankInfo::class,
                'label' => 'BankInfo id',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
            'allow_extra_fields' => true,
        ]);
    }
}