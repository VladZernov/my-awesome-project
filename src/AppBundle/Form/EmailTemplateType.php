<?php

namespace AppBundle\Form;

use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\File;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EmailTemplate
 */
class EmailTemplateType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => EmailTemplate::getTypesChoices(),
            ])
            ->add('subject')
            ->add('description')
            ->add('content')
            ->add('active', ChoiceType::class, [
                'choices' => [true, false],
            ])
            ->add('attachments', EntityType::class, [
                'class' => File::class,
                'label' => 'File id',
                'multiple' => true,
                'by_reference' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmailTemplate::class,
            'allow_extra_fields' => true,
        ]);
    }
}