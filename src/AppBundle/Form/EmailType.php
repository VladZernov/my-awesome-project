<?php

namespace AppBundle\Form;

use AppBundle\Entity\Email;
use AppBundle\Entity\File;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaymentType
 */
class EmailType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject')
            ->add('message')
            ->add('recipients', CollectionType::class, [
                'entry_type' => EmailRecipientType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('attachments', EntityType::class, [
                'class' => File::class,
                'label' => 'File id',
                'multiple' => true,
                'by_reference' => false,
            ])
            ->add('status');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Email::class,
            'allow_extra_fields' => true,
        ]);
    }
}