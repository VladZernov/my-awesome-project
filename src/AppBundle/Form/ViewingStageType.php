<?php

namespace AppBundle\Form;

use AppBundle\Entity\ViewingStage;
use AppBundle\Validator\ViewingStageAllowed;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ViewingStageType
 */
class ViewingStageType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('label')
            ->add('order')
            ->add('flowFlag', ChoiceType::class, [
                'choices' => [
                    ViewingStage::START_STAGE,
                    ViewingStage::MIDDLE_STAGE,
                    ViewingStage::FINISH_STAGE,
                ],
                'description' => 'Final state on which we should',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ViewingStage::class,
            'allow_extra_fields' => true,
            'constraints' => new ViewingStageAllowed(),
        ]);
    }
}