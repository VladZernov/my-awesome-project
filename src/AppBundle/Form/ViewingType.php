<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Unit;
use AppBundle\Entity\Viewing;
use AppBundle\Entity\ViewingStage;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ViewingType
 */
class ViewingType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price')
            ->add('notes')
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
            ])
            ->add('contact', EntityType::class, [
                'class' => Contact::class,
                'label' => 'Contact id',
            ])
            ->add('unit', EntityType::class, [
                'class' => Unit::class,
                'label' => 'Unit id',
            ])
            ->add('stage', EntityType::class, [
                'class' => ViewingStage::class,
                'label' => 'Viewing Stage id',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Viewing::class,
            'allow_extra_fields' => true,
        ]);
    }
}