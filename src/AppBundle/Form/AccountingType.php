<?php

namespace AppBundle\Form;

use AppBundle\Entity\Accounting;
use AppBundle\Entity\ExpenseType;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaymentType
 */
class AccountingType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expenseType', EntityType::class, [
                'class' => ExpenseType::class,
                'label' => 'Expense type id',
            ])
            ->add('amount')
            ->add('notes')
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Accounting::class,
            'allow_extra_fields' => true,
        ]);
    }
}