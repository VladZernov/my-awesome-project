<?php

namespace AppBundle\Form;

use AppBundle\Entity\File;
use AppBundle\Entity\Property;
use AppBundle\Entity\Unit;
use Requestum\ApiBundle\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UnitType
 */
class UnitType extends AbstractApiType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('modelPrice')
            ->add('targetPrice')
            ->add('doorCode')
            ->add('wifi')
            ->add('notes')
            ->add('grossSize', IntegerType::class)
            ->add('netSize', IntegerType::class)
            ->add('notes')
            ->add('bedSize')
            ->add('property', EntityType::class, [
                'class' => Property::class,
                'label' => 'Property id',
            ])
            ->add('inventoryItems', CollectionType::class, [
                'entry_type' => InventoryItemType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('repairs', CollectionType::class, [
                'entry_type' => UnitRepairType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('files', EntityType::class, [
                'class' => File::class,
                'multiple' => true,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Unit::class,
            'allow_extra_fields' => true,
        ]);
    }
}