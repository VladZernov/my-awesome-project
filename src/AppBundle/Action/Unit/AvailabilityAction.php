<?php

namespace AppBundle\Action\Unit;

use AppBundle\Entity\Unit;
use Requestum\ApiBundle\Action\ListAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class AvailabilityAction
 */
class AvailabilityAction extends ListAction
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function executeAction(Request $request)
    {

        $filters = $this->getFilters($request);

        $from = $this->extractParam($filters, 'from', null);
        $to = $this->extractParam($filters, 'to', null);

        if (!$from || !$to) {
            throw new BadRequestHttpException('Some of parameters are not provided');
        }

        $data = $this->getDoctrine()->getRepository(Unit::class)->getAvailabilitySchedule($from, $to);

        return $this->handleResponse($data, Response::HTTP_OK);
    }
}