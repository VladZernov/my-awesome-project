<?php

namespace AppBundle\Action;

use Requestum\ApiBundle\Filter\Exception\BadFilterException;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\InvalidArgumentException;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Requestum\ApiBundle\Action\ListAction as BaseListAction;

/**
 * Class ListAction
 */
class ListAction extends BaseListAction
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function executeAction(Request $request)
    {
        $filters = $this->getFilters($request);

        $page = $this->extractParam($filters, 'page', 1);
        $perPage = $this->extractParam($filters, 'per-page', $this->options['default_per_page']);
        $expandExpression = $this->extractParam($filters, 'expand', null);
        $expand = $expandExpression ? explode(',', $expandExpression) : [];

        try {
            $entitiesQueryBuilder = $this->createQueryBuilder($filters);
        } catch (BadFilterException $exception) {
            $value = $exception->getValue();

            if (is_scalar($value)) {
                $message = sprintf('Unprocessable filter "%s" with value "%s".', $exception->getName(), $value);
            } else {
                $value = is_object($value) ? get_class($value) : gettype($value);
                $message = sprintf('Unprocessable filter "%s" of type "%s".', $exception->getName(), $value);
            }

            if ($exception->getMessage()) {
                $message = sprintf("%s %s.", $message, $exception->getMessage());
            }

            throw new BadRequestHttpException($message);
        }

        try {
            $result = new Pagerfanta(new DoctrineORMAdapter($entitiesQueryBuilder, true));
            $result
                ->setMaxPerPage($perPage)
                ->setCurrentPage($page)
            ;
        } catch (InvalidArgumentException $exception) {
            throw new NotFoundHttpException();
        }

        if ($request->attributes->get('count-only')) {
            $result = ['total' => $result->getNbResults()];
        }

        return $this->handleResponse($result, Response::HTTP_OK, [
            'expand' => $expand,
        ]);
    }
}