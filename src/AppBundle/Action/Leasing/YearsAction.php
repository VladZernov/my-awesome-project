<?php

namespace AppBundle\Action\Leasing;

use AppBundle\Entity\Leasing;
use Requestum\ApiBundle\Action\ListAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class YearsAction
 */
class YearsAction extends ListAction
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function executeAction(Request $request)
    {

        $data = $this->getDoctrine()->getRepository(Leasing::class)->getYears();

        return $this->handleResponse($data, Response::HTTP_OK);
    }
}