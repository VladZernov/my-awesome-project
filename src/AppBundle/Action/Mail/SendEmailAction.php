<?php

namespace AppBundle\Action\Mail;

use Requestum\ApiBundle\Action\CreateAction;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SendEmailAction
 */
class SendEmailAction extends CreateAction
{
    /**
     * @param Request $request
     * @param object  $email
     * @param Form    $form
     *
     * @return void|mixed
     *
     * @throws \Exception
     */
    protected function processSubmit(Request $request, $email, Form $form)
    {
        $this->get('app.service.email.email_manager')->sendEmail($email);

        $em = $this->getDoctrine()->getManager();
        $em->persist($email);
        $em->flush();
    }
}