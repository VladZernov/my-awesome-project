<?php

namespace AppBundle\Action\Statistics;

use AppBundle\Entity\ContactCampaignSource;
use AppBundle\Repository\ContactCampaignSourceRepository;
use Requestum\ApiBundle\Action\ListAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StatisticsCampaignSourcesAction
 */
class StatisticsCampaignSourcesAction extends ListAction
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function executeAction(Request $request)
    {

        $filters = $this->getFilters($request);

        $from = $this->extractParam($filters, 'from', null);
        $to = $this->extractParam($filters, 'to', null);

        /** @var ContactCampaignSourceRepository $repository */
        $repository = $this->getDoctrine()->getRepository(ContactCampaignSource::class);

        $data = $repository->getStatisticsForPeriod($from, $to);

        return $this->handleResponse($data, Response::HTTP_OK);
    }
}