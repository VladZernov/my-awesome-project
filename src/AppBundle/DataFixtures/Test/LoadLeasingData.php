<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\DataFixtures\ORM\LoadLeasingTypeData;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Leasing;
use AppBundle\Entity\LeasingType;
use AppBundle\Entity\Unit;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class LoadViewingData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadLeasingData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadLeasingTypeData::class,
            LoadViewingData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var LeasingType $type */
        $type = $this->getReference('leasing-type-nr');

        /** @var Contact $contact */
        $contact = $this->getReference('contact-0');

        /** @var Unit $unit */
        $unit = $this->getReference('unit-0');

        $leasing = new Leasing();
        $leasing->setType($type)
            ->setContact($contact)
            ->setUnit($unit)
            ->setPrice(10000)
            ->setStartDate(new \DateTime('+20 day'))
            ->setPayDay(15)
            ->setEndDate(new \DateTime('+6 month'))
            ->setSecurityDepositAmount(20000)
            ->setNotes('test leasing');

        $this->setReference('leasing-0', $leasing);

        $manager->persist($leasing);

        $manager->flush();
    }
}