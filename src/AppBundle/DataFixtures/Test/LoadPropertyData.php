<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\DataFixtures\ORM\LoadBankData;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use AppBundle\DataFixtures\ORM\LoadAccessTokenData;
use AppBundle\Entity\Property;

/**
 * Class LoadPropertyData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadPropertyData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadAccessTokenData::class,
            LoadBankData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity = new Property();
        $entity
            ->setName('Sparrow Hills')
            ->setAddress('Hollywood 1')
            ->setCity('London')
            ->setZipcode(123456)
            ->setBankInfo($this->getReference('bank-0'));

        $this->setReference('building-sparrow-hills', $entity);

        $manager->persist($entity);

        $entity = new Property();
        $entity
            ->setName('Red Mountain')
            ->setAddress('Hollywood 2')
            ->setCity('London')
            ->setZipcode(123457)
            ->setBankInfo($this->getReference('bank-1'));

        $this->setReference('red-mountain-hills', $entity);

        $manager->persist($entity);

        $entity = new Property();
        $entity
            ->setName('First capital')
            ->setAddress('Hollywood 3')
            ->setCity('London')
            ->setZipcode(123458)
            ->setBankInfo($this->getReference('bank-1'));

        $this->setReference('first-capital-building', $entity);

        $manager->persist($entity);

        $manager->flush();
    }
}