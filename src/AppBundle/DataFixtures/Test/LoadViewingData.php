<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\DataFixtures\ORM\LoadViewingStagesData;
use AppBundle\Entity\Viewing;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class LoadViewingData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadViewingData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadUnitData::class,
            LoadContactData::class,
            LoadViewingStagesData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $stage = $this->getReference('stage_scheduled');
        $manager->persist($stage);

        $entity = new Viewing();
        $entity->setPrice(1000);
        $entity->setNotes('testViewing1Note');
        $entity->setContact($this->getReference('contact-0'));
        $entity->setUnit($this->getReference('unit-1'));
        $entity->setDate(new \DateTime('20.05.2018'));
        $entity->setStage($stage);

        $this->setReference('viewing-0', $entity);

        $manager->persist($entity);

        $stage = $this->getReference('stage_contract_won');
        $manager->persist($stage);

        $entity = new Viewing();
        $entity->setPrice(1000);
        $entity->setNotes('testViewing2Note');
        $entity->setContact($this->getReference('contact-0'));
        $entity->setUnit($this->getReference('unit-0'));
        $entity->setDate(new \DateTime('28.05.2018'));
        $entity->setStage($stage);

        $this->setReference('viewing-1', $entity);

        $manager->persist($entity);

        $entity = new Viewing();
        $entity->setPrice(1000);
        $entity->setNotes('testViewing2Note');
        $entity->setContact($this->getReference('contact-1'));
        $entity->setUnit($this->getReference('unit-2'));
        $entity->setDate(new \DateTime('28.05.2018'));
        $entity->setStage($stage);

        $this->setReference('viewing-2', $entity);

        $manager->persist($entity);

        $manager->flush();
    }
}