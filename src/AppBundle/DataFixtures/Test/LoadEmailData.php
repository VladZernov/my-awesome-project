<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\Entity\Email;
use AppBundle\Entity\EmailRecipient;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class LoadContactData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadEmailData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadContactData::class,
            LoadFileData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity = new Email();
        $entity->setSubject('testSubject1');
        $entity->setMessage('testMessage1');
        $entity->setStatus('New');
        $entity->addRecipient((new EmailRecipient())->setContact($this->getReference('contact-1')));
        $entity->addAttachment($this->getReference('file-1'));

        $manager->persist($entity);

        $entity = new Email();
        $entity->setSubject('testSubject2');
        $entity->setMessage('testMessage2');
        $entity->setStatus('New');
        $entity->addRecipient((new EmailRecipient())->setContact($this->getReference('contact-1')));
        $entity->addAttachment($this->getReference('file-2'));

        $manager->persist($entity);

        $manager->flush();
    }
}