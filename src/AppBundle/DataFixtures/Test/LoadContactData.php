<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\DataFixtures\ORM\LoadCampaignSourcesData;
use AppBundle\DataFixtures\ORM\LoadContactGroupData;
use AppBundle\Entity\Contact;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use AppBundle\DataFixtures\ORM\LoadAccessTokenData;

use AppBundle\Entity\Property;

/**
 * Class LoadContactData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadContactData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadAccessTokenData::class,
            LoadCampaignSourcesData::class,
            LoadContactGroupData::class,
            LoadFileData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity = new Contact();
        $entity->setFirstName('John');
        $entity->setLastName('Doe');
        $entity->setEmail('john.doe@email.com');
        $entity->setBudget(1200);
        $entity->setEmployer('Google');
        $entity->setPhone(34643563454);
        $entity->setProfession('Programmer');
        $entity->setCampaignSource($this->getReference('source-0'));
        $entity->addDocument($this->getReference('file-0'));
        $entity->addGroup($this->getReference('group-0'));

        $this->setReference('contact-0', $entity);

        $manager->persist($entity);

        $entity = new Contact();
        $entity->setFirstName('Bob');
        $entity->setLastName('Doe');
        $entity->setEmail('bob.doe@email.com');
        $entity->setBudget(1200);
        $entity->setEmployer('Google');
        $entity->setPhone(31643563454);
        $entity->setProfession('Programmer');
        $entity->setCampaignSource($this->getReference('source-0'));
        $entity->addDocument($this->getReference('file-1'));
        $entity->addGroup($this->getReference('group-0'));

        $this->setReference('contact-1', $entity);

        $manager->persist($entity);

        $entity = new Contact();
        $entity->setFirstName('Dale');
        $entity->setLastName('Doe');
        $entity->setEmail('dale.doe@email.com');
        $entity->setBudget(1200);
        $entity->setEmployer('Google');
        $entity->setPhone(33643563454);
        $entity->setProfession('Programmer');
        $entity->setCampaignSource($this->getReference('source-0'));
        $entity->addDocument($this->getReference('file-2'));
        $entity->addGroup($this->getReference('group-0'));

        $this->setReference('contact-2', $entity);

        $manager->persist($entity);


        $manager->flush();
    }
}