<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\Entity\Unit;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class LoadUnitData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadUnitData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadPropertyData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity = new Unit();
        $entity->setName('A');
        $entity->setBedSize('big');
        $entity->setDoorCode('123');
        $entity->setGrossSize(44);
        $entity->setModelPrice(1200);
        $entity->setNetSize(49);
        $entity->setNotes('test');
        $entity->setTargetPrice(1222);
        $entity->setWifi('ddd');
        $entity->setProperty($this->getReference('first-capital-building'));

        $this->setReference('unit-0', $entity);

        $manager->persist($entity);

        $entity = new Unit();
        $entity->setName('B');
        $entity->setBedSize('big');
        $entity->setDoorCode('123');
        $entity->setGrossSize(44);
        $entity->setModelPrice(900);
        $entity->setNetSize(49);
        $entity->setNotes('test2');
        $entity->setTargetPrice(1222);
        $entity->setWifi('ddd');
        $entity->setProperty($this->getReference('first-capital-building'));

        $this->setReference('unit-1', $entity);

        $manager->persist($entity);

        $entity = new Unit();
        $entity->setName('C');
        $entity->setBedSize('big');
        $entity->setDoorCode('123');
        $entity->setGrossSize(44);
        $entity->setModelPrice(1000);
        $entity->setNetSize(49);
        $entity->setNotes('test3');
        $entity->setTargetPrice(1222);
        $entity->setWifi('wf');
        $entity->setProperty($this->getReference('first-capital-building'));

        $this->setReference('unit-2', $entity);

        $manager->persist($entity);

        $manager->flush();
    }
}