<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\DataFixtures\ORM\LoadPaymentTypeData;
use AppBundle\Entity\Leasing;
use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class LoadViewingData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadPaymentData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadPaymentTypeData::class,
            LoadLeasingData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var Leasing $leasing */
        $leasing = $this->getReference('leasing-0');

        /** @var PaymentType $type */
        $type = $this->getReference('payment-type-rent');

        $payment = new Payment();
        $payment->setAmount(14830)
            ->setLeasing($leasing)
            ->setType($type)
            ->setTransactionType(Payment::TRANSACTION_CREDIT)
            ->setDueDate($leasing->getCurrentPayDay())
            ->setPaymentMethod(Payment::METHOD_CARD);

        $this->setReference('payment-0', $payment);
        $manager->persist($payment);

        $payment = new Payment();
        $payment->setAmount(10000)
            ->setLeasing($leasing)
            ->setType($type)
            ->setTransactionType(Payment::TRANSACTION_CREDIT)
            ->setDueDate((clone $leasing->getCurrentPayDay())->modify('+1 month'))
            ->setPaymentMethod(Payment::METHOD_CARD);

        $this->setReference('payment-1', $payment);
        $manager->persist($payment);

        $manager->flush();
    }
}