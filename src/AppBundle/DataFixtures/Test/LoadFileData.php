<?php

namespace AppBundle\DataFixtures\Test;

use AppBundle\Entity\File;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadFileData
 *
 * @package AppBundle\DataFixtures\Test
 */
class LoadFileData extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $file = new File();

        $file->setName('doc0.png')
            ->setPath('docs/doc0.png')
            ->setContentType('image/png')
            ->setSize(42);

        $this->setReference('file-0', $file);

        $manager->persist($file);

        $file = new File();

        $file->setName('doc1.png')
            ->setPath('docs/doc1.png')
            ->setContentType('image/png')
            ->setSize(42);

        $this->setReference('file-1', $file);

        $manager->persist($file);

        $file = new File();

        $file->setName('doc2.png')
            ->setPath('docs/doc2.png')
            ->setContentType('image/png')
            ->setSize(42);

        $this->setReference('file-2', $file);

        $manager->persist($file);

        $manager->flush();
    }
}