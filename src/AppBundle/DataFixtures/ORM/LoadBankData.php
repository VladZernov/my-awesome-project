<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\BankInfo;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadBankData.
 */
class LoadBankData extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $bank = new BankInfo();
        $bank->setName('Standard Chartered Bank');
        $bank->setAccountName('ACJ Ventures Limited ');
        $bank->setAccountNumber('40700815929');
        $bank->setBranchCode('407');
        $bank->setCode('003');
        $bank->setSwift('SCBLHKHHXXX');
        $this->addReference('bank-0', $bank);

        $manager->persist($bank);

        $bank = new BankInfo();
        $bank->setName('SHSBC');
        $bank->setAccountName('Tane Residence Limited');
        $bank->setAccountNumber('741-084602-001');
        $bank->setBranchCode('741');
        $bank->setCode('004');
        $bank->setSwift('HSBCHKHHHKH');
        $this->addReference('bank-1', $bank);

        $manager->persist($bank);

        $manager->flush();
    }
}
