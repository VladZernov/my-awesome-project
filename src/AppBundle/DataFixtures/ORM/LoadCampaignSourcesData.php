<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ContactCampaignSource;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\InventoryType;

/**
 * Class LoadCampaignSourcesData
 */
class LoadCampaignSourcesData extends AbstractFixture
{

    private $sources = [
        'facebook',
        'google',
        'twitter',
        'instagram',
        'linkedin',
    ];

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        foreach ($this->sources as $key => $source) {
            $campaignSource = new ContactCampaignSource();
            $campaignSource->setName($source);

            $this->addReference('source-'.$key, $campaignSource);

            $manager->persist($campaignSource);
        }

        $manager->flush();
    }
}
