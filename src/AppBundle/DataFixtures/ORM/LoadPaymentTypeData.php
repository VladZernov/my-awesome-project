<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\PaymentType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadPaymentTypeData
 */
class LoadPaymentTypeData extends AbstractFixture
{

    private $types = [
        PaymentType::RENT_TYPE,
        'electricity',
        'utility',
    ];

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->types as $type) {
            $typeEntity = new PaymentType();
            $typeEntity->setName($type);

            $this->setReference('payment-type-'.$type, $typeEntity);

            $manager->persist($typeEntity);
        }

        $manager->flush();
    }
}
