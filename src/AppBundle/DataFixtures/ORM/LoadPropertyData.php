<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use AppBundle\Entity\Property;

/**
 * Class LoadPropertyData
 */
class LoadPropertyData extends AbstractFixture implements DependentFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadBankData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $property = new Property();
            $property
                ->setName('Property name '.$i)
                ->setAddress('Property address '.$i)
                ->setCity('Property city '.$i)
                ->setZipcode(123)
                ->setBankInfo($this->getReference('bank-'.($i % 2 ? 0:1)))
            ;

            $manager->persist($property);

            $this->addReference('property-'.$i, $property);
        }

        $manager->flush();
    }
}
