<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\ViewingStage;

/**
 * Class LoadViewingStagesData
 */
class LoadViewingStagesData extends AbstractFixture
{

    private $stages = [
        'scheduled' => 'Scheduled',
        'viewed' => 'Viewed',
        'contract_sent' => 'Contract Sent',
        'contract_signed' => 'Contract Signed',
        'deposit_paid' => 'Deposit Paid',
        'contract_lost' => 'Contract Lost',
        'contract_won' => 'Contract Won',
    ];

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $order = 1;

        foreach ($this->stages as $key => $stageName) {
            $stage = new ViewingStage();
            $stage
                ->setName($key)
                ->setLabel($stageName)
                ->setOrder($order)
            ;

            if($key == 'scheduled') {
                $stage->setFlowFlag(ViewingStage::START_STAGE);
            }

            if($key == 'contract_won') {
                $stage->setFlowFlag(ViewingStage::FINISH_STAGE);
            }

            $this->addReference('stage_'.$key, $stage);

            $manager->persist($stage);
            $order++;
        }

        $manager->flush();
    }
}
