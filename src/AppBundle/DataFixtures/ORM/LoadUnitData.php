<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use AppBundle\Entity\Unit;

/**
 * Class LoadUnitData
 */
class LoadUnitData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadPropertyData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $unit = new Unit();
            $unit
                ->setName('Unit name '.$i)
                ->setDoorCode(123)
                ->setProperty($this->getReference('property-'.$i))
            ;

            $manager->persist($unit);

            $this->addReference('unit-1'.$i, $unit);
        }

        $manager->flush();
    }
}
