<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\EmailTemplate;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadTemplateData
 */
class LoadTemplateData extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $template = new EmailTemplate();
        $template->setType(EmailTemplate::REGISTRATION_TYPE);
        $template->setSubject('Account created');
        $template->setDescription('User account was successfully created');
        $template->setContent(file_get_contents(__DIR__.'/email_template/registered.html'));
        $template->setActive(true);

        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::CHECKIN_TYPE);
        $template->setSubject('Checkin');
        $template->setContent(file_get_contents(__DIR__.'/email_template/checkin.html'));
        $template->setActive(true);
        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::REMIND_TYPE);
        $template->setSubject('Invoice');
        $template->setContent(file_get_contents(__DIR__.'/email_template/remind.html'));
        $template->setActive(true);

        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::RECEIPT_TYPE);
        $template->setSubject('Receipt');
        $template->setContent(file_get_contents(__DIR__.'/email_template/receipt.html'));
        $template->setActive(true);

        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::CHECKIN_REMIND_TYPE);
        $template->setSubject('Checkin remind');
        $template->setContent(file_get_contents(__DIR__.'/email_template/checkin_remind.html'));
        $template->setActive(true);

        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::CHECKOUT_TYPE);
        $template->setSubject('Checkout');
        $template->setContent(file_get_contents(__DIR__.'/email_template/checkout.html'));
        $template->setActive(true);

        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::INVOICE_TYPE);
        $template->setSubject('Payment invoice');
        $template->setContent(file_get_contents(__DIR__.'/email_template/invoice.html'));
        $template->setActive(true);

        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::LATE_TYPE);
        $template->setSubject('Payment delay');
        $template->setContent(file_get_contents(__DIR__.'/email_template/late.html'));
        $template->setActive(true);

        $manager->persist($template);

        $template = new EmailTemplate();
        $template->setType(EmailTemplate::RENEW_TYPE);
        $template->setSubject('Leasing renew');
        $template->setContent(file_get_contents(__DIR__.'/email_template/renew.html'));
        $template->setActive(true);

        $manager->persist($template);

        $manager->flush();
    }
}
