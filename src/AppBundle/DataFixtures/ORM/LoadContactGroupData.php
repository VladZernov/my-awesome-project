<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ContactGroup;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadContactGroupData
 */
class LoadContactGroupData extends AbstractFixture
{

    private $groups = [
        'group1',
        'group2',
        'group3',
    ];

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->groups as $key => $group) {
            $contactGroup = new ContactGroup();
            $contactGroup->setName($group);

            $this->addReference('group-'.$key, $contactGroup);

            $manager->persist($contactGroup);
        }

        $manager->flush();
    }
}
