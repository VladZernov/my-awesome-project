<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use AppBundle\Entity\Leasing;

/**
 * Class LoadLeasingData
 */
class LoadLeasingData extends AbstractFixture implements DependentFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function getDependencies()
    {
        return [
            LoadContactData::class,
            LoadUnitData::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $startDate = new \DateTime();
            $startDate->sub(new \DateInterval('P1D'));

            $endDate = new \DateTime();
            $endDate->add(new \DateInterval('P'.($i * 2).'M'));

            $leasing = new Leasing();
            $leasing
                ->setPrice(10 * $i)
                ->setContact($this->getReference('contact-1'.$i))
                ->setUnit($this->getReference('unit-1'.$i))
                ->setStartDate($startDate)
                ->setEndDate($endDate)
                ->setType($this->getReference('leasing-type-nr'))
            ;

            $manager->persist($leasing);
        }

        $manager->flush();
    }
}
