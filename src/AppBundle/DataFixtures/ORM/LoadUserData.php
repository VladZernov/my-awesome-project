<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadUserData
 */
class LoadUserData extends AbstractFixture
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('alex@gmail.com');
        $user->setFullName('Alex');
        $user->setPlainPassword('qwerty123');
        $user->setEnabled(true);
        $this->addReference('user-artur', $user);

        $manager->persist($user);

        $user = new User();
        $user->setEmail('kirill@gmail.com');
        $user->setFullName('Kirill');
        $user->setPlainPassword('qwerty123');
        $user->setEnabled(true);
        $this->addReference('user-kirill', $user);

        $manager->persist($user);

        $manager->flush();
    }
}
