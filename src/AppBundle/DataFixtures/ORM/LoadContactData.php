<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Contact;

/**
 * Class LoadContactData
 */
class LoadContactData extends AbstractFixture
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $contact = new Contact();
            $contact
                ->setFirstName('Contact first name '.$i)
                ->setLastName('Contact last name '.$i)
                ->setEmail('contact-'.$i.'@gmail.com')
                ->setPhone('12344567')
                ->setBudget(100)
                ->setEmployer('Employer '.$i)
                ->setProfession('Profession '.$i)
                ->setGender('male')
                ->setAge(25)
                ->setSalary(1000)
                ->setActive(1)
            ;

            $manager->persist($contact);

            $this->addReference('contact-1'.$i, $contact);
        }

        $manager->flush();
    }
}
