<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\EmailTemplateParameter;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadTemplateParametersData
 */
class LoadTemplateParametersData extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::BANK_ACCOUNT_NAME);
        $parameter->setLabel('Bank account name');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::BANK_ACCOUNT_NUMBER);
        $parameter->setLabel('Bank account number');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::BANK_BRANCH_CODE);
        $parameter->setLabel('Bank branch code');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::BANK_CODE);
        $parameter->setLabel('Bank code');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::BANK_NAME);
        $parameter->setLabel('Bank name');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::CHECK_IN_DATE);
        $parameter->setLabel('Check-in date');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::CONTACT_FULL_NAME);
        $parameter->setLabel('Contact full name');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::DEPARTURE_DATE);
        $parameter->setLabel('Departure date');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::DOOR_CODE);
        $parameter->setLabel('Door code');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::PAYMENT_MONTH);
        $parameter->setLabel('Payment month');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::RENT_AMOUNT);
        $parameter->setLabel('Rent amount');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::PROPERTY_ADDRESS);
        $parameter->setLabel('Property address');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::RENT_DUE_DATE);
        $parameter->setLabel('Rent due date');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::SWIFT);
        $parameter->setLabel('SWIFT');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::UNIT_NAME);
        $parameter->setLabel('Building & Unit #');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::WIFI);
        $parameter->setLabel('Wifi');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::OUTSTANDINGS);
        $parameter->setLabel('Outstandings');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::SECURITY_DEPOSIT_AMOUNT);
        $parameter->setLabel('Security deposit amount');

        $manager->persist($parameter);

        $parameter = new EmailTemplateParameter();
        $parameter->setName(EmailTemplateParameter::MONTHLY_UTILITY_CREDIT);
        $parameter->setLabel('Monthly utility credit');

        $manager->persist($parameter);

        $manager->flush();
    }
}
