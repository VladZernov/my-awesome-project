<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\LeasingType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadLeasingTypeData
 */
class LoadLeasingTypeData extends AbstractFixture
{

    private $types = [
        'NR',
        'MR',
        'MTM',
        '1MN',
    ];

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->types as $type) {
            $typeEntity = new LeasingType();
            $typeEntity->setName($type);

            $manager->persist($typeEntity);

            $this->addReference('leasing-type-'.strtolower($type), $typeEntity);
        }

        $manager->flush();
    }
}
