<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UnitController extends BaseController
{
    /**
     * @Route(path="/units", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of units",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"},
     *          {"name"="leasings_date[from]", "dataType"="array", "required"=false, "description"="Get all units in date (format:Y.m)"},
     *          {"name"="leasings_date[to]", "dataType"="array", "required"=false, "description"="Get all units in date (format:Y.m)"},
     *          {"name"="leasings[id]", "dataType"="int", "required"=false, "description"="Leasing Id"},
     *          {"name"="leasings[name]", "dataType"="string", "required"=false, "description"="Leasing name"},
     *          {"name"="query", "dataType"="string", "required"=false, "description"="search query, supports wildcards (*suffix, prefix*, *middle*)"},
     *          {"name"="available", "dataType"="boolean", "required"=false, "description"="Is unit available for leasing?"},
     *          {"name"="available_on[from]", "dataType"="string", "required"=false, "description"="Unit available from (format:Y.m)"},
     *          {"name"="available_on[to]", "dataType"="string", "required"=false, "description"="Unit available to (format:Y.m)"},
     *     },
     *     section = "Units",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.unit.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets an availability statistics for period",
     *     output = "Unit",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="from", "dataType"="date", "required"=true, "description"="Date in format 'yyyy.mm'"},
     *          {"name"="to", "dataType"="date", "required"=true, "description"="Date in format 'yyyy.mm'"},
     *     },
     *     section = "Units",
     * )
     *
     * @Route(path="/units/availability", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function availabilityAction()
    {
        return $this->action("action.unit.availability");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new unit from the submitted data.",
     *     input = "AppBundle\Form\UnitType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Units",
     * )
     *
     * @Route(path="/units", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.unit.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update unit from the submitted data.",
     *     input = "AppBundle\Form\UnitType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Units",
     * )
     *
     * @Route(path="/units/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.unit.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a unit by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Units",
     * )
     *
     * @Route(path="/units/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.unit.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a unit for a given id",
     *     output = "Unit",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Units",
     * )
     *
     * @Route(path="/units/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.unit.fetch");
    }
}