<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ViewingStageController extends BaseController
{
    /**
     * @Route(path="/viewings-stages", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of viewings stages",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="name", "dataType"="string", "required"=false, "description"="Name"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Viewings Stages",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.viewing_stage.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new viewing stage from the submitted data.",
     *     input = "AppBundle\Form\ViewingStageType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Viewings Stages",
     * )
     *
     * @Route(path="/viewings-stages", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.viewing_stage.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update viewing stage from the submitted data.",
     *     input = "AppBundle\Form\ViewingStageType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Viewings Stages",
     * )
     *
     * @Route(path="/viewings-stages/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.viewing_stage.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a viewing stage by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Viewings Stages",
     * )
     *
     * @Route(path="/viewings-stages/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.viewing_stage.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a viewing stage for a given id",
     *     output = "ViewingStage",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Viewings Stages",
     * )
     *
     * @Route(path="/viewings-stages/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.viewing_stage.fetch");
    }
}