<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class LeasingController extends BaseController
{
    /**
     * @Route(path="/leasings", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of leasings",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="price", "dataType"="int", "required"=false, "description"="Price"},
     *          {"name"="unit", "dataType"="int", "required"=false, "description"="Unit id"},
     *          {"name"="contact", "dataType"="int", "required"=false, "description"="Contact id"},
     *          {"name"="endDate", "dataType"="string", "required"=false, "description"="Date of checkout in format 'yyyy.mm.dd', >,<,>=,<= can be used"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Leasings",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.leasing.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new leasing from the submitted data.",
     *     input = "AppBundle\Form\LeasingType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Leasings",
     * )
     *
     * @Route(path="/leasings", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.leasing.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update leasing from the submitted data.",
     *     input = "AppBundle\Form\LeasingType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Leasings",
     * )
     *
     * @Route(path="/leasings/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.leasing.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a leasing by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Leasings",
     * )
     *
     * @Route(path="/leasings/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.leasing.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a leasing for a given id",
     *     output = "Leasing",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Leasings",
     * )
     *
     * @Route(path="/leasings/{id}", methods={"GET"}, requirements={"id"="\d+"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.leasing.fetch");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets leasings years",
     *     output = "Leasing",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     section = "Leasings",
     * )
     *
     * @Route(path="/leasings/years", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function yearsAction()
    {
        return $this->action("action.leasing.years");
    }
}