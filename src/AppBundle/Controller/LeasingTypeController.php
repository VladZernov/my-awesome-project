<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class LeasingTypeController extends BaseController
{
    /**
     * @Route(path="/leasing-types", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of leasing types",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Leasing Types",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.leasing_type.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new leasing type from the submitted data.",
     *     input = "AppBundle\Form\LeasingTypeForm",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Leasing Types",
     * )
     *
     * @Route(path="/leasing-types", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.leasing_type.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update leasing type from the submitted data.",
     *     input = "AppBundle\Form\LeasingTypeForm",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Leasing Types",
     * )
     *
     * @Route(path="/leasing-types/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.leasing_type.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a leasing type by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Leasing Types",
     * )
     *
     * @Route(path="/leasing-types/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.leasing_type.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a leasing type for a given id",
     *     output = "Leasing Type",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Leasing Types",
     * )
     *
     * @Route(path="/leasing-types/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.leasing_type.fetch");
    }
}