<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class StatisticsController extends BaseController
{
    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets statistics for campaign sources earnings by period",
     *     output = "Statistics",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *     parameters={
     *          {"name"="from", "dataType"="string", "required"=true, "description"="Start of period in format 'yyyy.mm.dd'"},
     *          {"name"="to", "dataType"="string", "required"=true, "description"="End of period in format 'yyyy.mm.dd'"},
     *     },
     *     section = "Statistics",
     * )
     *
     * @Route(path="/statistics/campaign-sources", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchCampaignSourcesStatisticsAction()
    {
        return $this->action("action.statistics.campaign_sources.list");
    }
}