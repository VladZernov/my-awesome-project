<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PaymentController extends BaseController
{
    /**
     * @Route(path="/payments", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of payments",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="dueDate", "dataType"="date", "required"=false, "description"="Due date"},
     *          {"name"="paidDate", "dataType"="date", "required"=false, "description"="Paid date"},
     *          {"name"="payment_type", "dataType"="int", "required"=false, "description"="Payment Type Id"},
     *          {"name"="leasing", "dataType"="int", "required"=false, "description"="Leasing Id"},
     *          {"name"="leasing", "dataType"="int", "required"=false, "description"="Leasing Id"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="query", "dataType"="string", "required"=false, "description"="search query, supports wildcards (*suffix, prefix*, *middle*)"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Payments",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.payment.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new payment from the submitted data.",
     *     input = "AppBundle\Form\PaymentType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Payments",
     * )
     *
     * @Route(path="/payments", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.payment.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update payment from the submitted data.",
     *     input = "AppBundle\Form\PaymentType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Payments",
     * )
     *
     * @Route(path="/payments/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.payment.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a payment by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Payments",
     * )
     *
     * @Route(path="/payments/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.payment.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a payment for a given id",
     *     output = "Payment",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Payments",
     * )
     *
     * @Route(path="/payments/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.payment.fetch");
    }
}