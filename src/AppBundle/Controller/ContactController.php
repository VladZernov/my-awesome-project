<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ContactController extends BaseController
{
    /**
     * @Route(path="/contacts", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of contacts",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="campaign_source_name", "dataType"="string", "required"=false, "description"="Campaign source name"},
     *          {"name"="group_name", "dataType"="string", "required"=false, "description"="Group name"},
     *          {"name"="unit_name", "dataType"="string", "required"=false, "description"="Group name"},
     *          {"name"="property_name", "dataType"="string", "required"=false, "description"="Group name"},
     *          {"name"="email", "dataType"="string", "required"=false, "description"="Email"},
     *          {"name"="phone", "dataType"="string", "required"=false, "description"="Phone"},
     *          {"name"="active", "dataType"="boolean", "required"=false, "description"="Is contact active?"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="query", "dataType"="string", "required"=false, "description"="search query, supports wildcards (*suffix, prefix*, *middle*)"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Contacts",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.contact.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new contact from the submitted data.",
     *     input = "AppBundle\Form\ContactType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Contacts",
     * )
     *
     * @Route(path="/contacts", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.contact.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update contact from the submitted data.",
     *     input = "AppBundle\Form\ContactType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Contacts",
     * )
     *
     * @Route(path="/contacts/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.contact.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a contact by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Contacts",
     * )
     *
     * @Route(path="/contacts/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.contact.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a contact for a given id",
     *     output = "Contact",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Contacts",
     * )
     *
     * @Route(path="/contacts/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.contact.fetch");
    }
}