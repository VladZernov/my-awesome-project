<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ContactGroupController extends BaseController
{
    /**
     * @Route(path="/contacts-groups", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of contacts groups",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="name", "dataType"="string", "required"=false, "description"="Name"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Contacts Groups",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.contact_group.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new contact group from the submitted data.",
     *     input = "AppBundle\Form\ContactGroupType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Contacts Groups",
     * )
     *
     * @Route(path="/contacts-groups", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.contact_group.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update contact group from the submitted data.",
     *     input = "AppBundle\Form\ContactGroupType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Contacts Groups",
     * )
     *
     * @Route(path="/contacts-groups/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.contact_group.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a contact group by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Contacts Groups",
     * )
     *
     * @Route(path="/contacts-groups/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.contact_group.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a contact group for a given id",
     *     output = "ContactGroup",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Contacts Groups",
     * )
     *
     * @Route(path="/contacts-groups/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.contact_group.fetch");
    }
}