<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ViewingController extends BaseController
{
    /**
     * @Route(path="/viewings", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of viewings",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="property_name", "dataType"="string", "required"=false, "description"="Property name"},
     *          {"name"="contact_name", "dataType"="string", "required"=false, "description"="Contact name"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Viewings",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.viewing.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new viewing from the submitted data.",
     *     input = "AppBundle\Form\ViewingType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Viewings",
     * )
     *
     * @Route(path="/viewings", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.viewing.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update viewing from the submitted data.",
     *     input = "AppBundle\Form\ViewingType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Viewings",
     * )
     *
     * @Route(path="/viewings/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.viewing.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a viewing by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Viewings",
     * )
     *
     * @Route(path="/viewings/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.viewing.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a viewing for a given id",
     *     output = "Viewing",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Viewings",
     * )
     *
     * @Route(path="/viewings/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.viewing.fetch");
    }
}