<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ExpenseTypeController extends BaseController
{
    /**
     * @Route(path="/expense-types", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of expense types",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Expense Types",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.expense_type.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new expense type from the submitted data.",
     *     input = "AppBundle\Form\ExpenseTypeForm",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Expense Types",
     * )
     *
     * @Route(path="/expense-types", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.expense_type.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update expense type from the submitted data.",
     *     input = "AppBundle\Form\ExpenseTypeForm",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Expense Types",
     * )
     *
     * @Route(path="/expense-types/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.expense_type.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete an expense type by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Expense Types",
     * )
     *
     * @Route(path="/expense-types/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.expense_type.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets an expense type for a given id",
     *     output = "Expense Type",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Expense Types",
     * )
     *
     * @Route(path="/expense-types/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.expense_type.fetch");
    }
}