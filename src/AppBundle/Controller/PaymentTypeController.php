<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PaymentTypeController extends BaseController
{
    /**
     * @Route(path="/payment-types", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of payment types",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="name", "dataType"="string", "required"=false, "description"="Name"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Payment Types",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.payment_type.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new payment type from the submitted data.",
     *     input = "AppBundle\Form\PaymentTypeForm",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Payment Types",
     * )
     *
     * @Route(path="/payment-types", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.payment_type.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update payment type from the submitted data.",
     *     input = "AppBundle\Form\PaymentTypeForm",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Payment Types",
     * )
     *
     * @Route(path="/payment-types/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.payment_type.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a payment type by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Payment Types",
     * )
     *
     * @Route(path="/payment-types/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.payment_type.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a payment type for a given id",
     *     output = "Payment Type",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Payment Types",
     * )
     *
     * @Route(path="/payment-types/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.payment_type.fetch");
    }
}