<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class BankInfoController extends BaseController
{
    /**
     * @Route(path="/bank-info", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of bank-info",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="query", "dataType"="string", "required"=false, "description"="search query, supports wildcards (*suffix, prefix*, *middle*)"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Bank Info",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.bank_info.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new bank-info from the submitted data.",
     *     input = "AppBundle\Form\BankInfoType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Bank Info",
     * )
     *
     * @Route(path="/bank-info", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.bank_info.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update bank-info from the submitted data.",
     *     input = "AppBundle\Form\BankInfoType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Bank Info",
     * )
     *
     * @Route(path="/bank-info/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.bank_info.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a bank-info by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Bank Info",
     * )
     *
     * @Route(path="/bank-info/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.bank_info.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a bank-info for a given id",
     *     output = "BankInfo",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Bank Info",
     * )
     *
     * @Route(path="/bank-info/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.bank_info.fetch");
    }
}