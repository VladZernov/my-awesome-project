<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class AccountingController extends BaseController
{
    /**
     * @Route(path="/accountings", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of accountings",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Accountings",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.accounting.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new accountings from the submitted data.",
     *     input = "AppBundle\Form\AccountingType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Accountings",
     * )
     *
     * @Route(path="/accountings", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.accounting.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update accounting from the submitted data.",
     *     input = "AppBundle\Form\AccountingType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Accountings",
     * )
     *
     * @Route(path="/accountings/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.accounting.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete an accounting by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Accountings",
     * )
     *
     * @Route(path="/accountings/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.accounting.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets an accounting for a given id",
     *     output = "Accounting",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Accountings",
     * )
     *
     * @Route(path="/accountings/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.accounting.fetch");
    }
}