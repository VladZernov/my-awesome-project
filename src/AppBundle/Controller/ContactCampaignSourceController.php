<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ContactCampaignSourceController extends BaseController
{
    /**
     * @Route(path="/contacts-campaign-sources", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of contacts campaign sources",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="name", "dataType"="string", "required"=false, "description"="Name"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Contacts Campaign Sources",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.contact_campaign_source.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new contact campaign source from the submitted data.",
     *     input = "AppBundle\Form\ContactCampaignSourceType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Contacts Campaign Sources",
     * )
     *
     * @Route(path="/contacts-campaign-sources", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.contact_campaign_source.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update contact campaign source from the submitted data.",
     *     input = "AppBundle\Form\ContactCampaignSourceType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Contacts Campaign Sources",
     * )
     *
     * @Route(path="/contacts-campaign-sources/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.contact_campaign_source.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a contact campaign source by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Contacts Campaign Sources",
     * )
     *
     * @Route(path="/contacts-campaign-sources/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.contact_campaign_source.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a contact campaign source for a given id",
     *     output = "ContactCampaignSource",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Contacts Campaign Sources",
     * )
     *
     * @Route(path="/contacts-campaign-sources/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.contact_campaign_source.fetch");
    }
}