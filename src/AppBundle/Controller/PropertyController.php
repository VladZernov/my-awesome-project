<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PropertyController extends BaseController
{
    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of property",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"},
     *          {"name"="name", "dataType"="string", "required"=false, "description"="Name"},
     *          {"name"="query", "dataType"="string", "required"=false, "description"="search query, supports wildcards (*suffix, prefix*, *middle*)"},
     *          {"name"="leasings_date[from]", "dataType"="array", "required"=false, "description"="Get all proterties in date (forma:Y.m)"},
     *          {"name"="leasings_date[to]", "dataType"="array", "required"=false, "description"="Get all proterties in date (forma:Y.m)"}
     *     },
     *     section = "Property",
     * )
     *
     * @Route(path="/property", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.property.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new property from the submitted data.",
     *     input = "AppBundle\Form\PropertyType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Property",
     * )
     *
     * @Route(path="/property", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.property.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update property from the submitted data.",
     *     input = "AppBundle\Form\PropertyType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Property",
     * )
     *
     * @Route(path="/property/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.property.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a property by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Property",
     * )
     *
     * @Route(path="/property/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.property.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a property for a given id",
     *     output = "Property",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Property",
     * )
     *
     * @Route(path="/property/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.property.fetch");
    }
}