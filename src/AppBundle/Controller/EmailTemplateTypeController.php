<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class EmailTemplateTypeController extends BaseController
{
    /**
     * @Route(path="/email-templates", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of email templates",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="type", "dataType"="string", "required"=false, "description"="Type"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Email Templates",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.email_template.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new email template from the submitted data.",
     *     input = "AppBundle\Form\EmailTemplateType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Email Templates",
     * )
     *
     * @Route(path="/email-templates", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.email_template.create");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update email template from the submitted data.",
     *     input = "AppBundle\Form\EmailTemplateType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Email Templates",
     * )
     *
     * @Route(path="/email-templates/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.email_template.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete a email template by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Email Templates",
     * )
     *
     * @Route(path="/email-templates/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.email_template.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets a email template for a given id",
     *     output = "Email Template",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Email Templates",
     * )
     *
     * @Route(path="/email-templates/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.email_template.fetch");
    }
}