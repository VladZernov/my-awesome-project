<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class EmailController extends BaseController
{
    /**
     * @Route(path="/emails", methods={"GET"})
     *
     * @ApiDoc(
     *     resource = true,
     *     description = "Get list of emails",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         304 = "Returned from cache"
     *     },
     *      parameters={
     *          {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *          {"name"="per-page", "dataType"="int", "required"=false, "description"="Items on page count"},
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *          {"name"="order-by", "dataType"="string", "required"=false, "description"="sorting by fields (pattern: 'field|order' eg 'name|asc' )"}
     *     },
     *     section = "Emails",
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->action("action.email.list");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Creates a new email delivery.",
     *     input = "AppBundle\Form\EmailType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Emails",
     * )
     *
     * @Route(path="/emails", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        return $this->action("action.email.create");
    }


    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Update email from the submitted data.",
     *     input = "AppBundle\Form\EmailType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         422 = "Returned when the form has errors"
     *     },
     *     section = "Emails",
     * )
     *
     * @Route(path="/emails/{id}", methods={"PATCH"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction()
    {
        return $this->action("action.email.update");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Delete an email group by id.",
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when 'foreign key constraint violation'"
     *     },
     *     section = "Emails",
     * )
     *
     * @Route(path="/emails/{id}", methods={"DELETE"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->action("action.email.delete");
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Gets an email for a given id",
     *     output = "Email",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the entity is not found"
     *     },
     *     parameters={
     *          {"name"="expand", "dataType"="string", "required"=false, "description"="Comma separated fields list to expand"},
     *     },
     *     section = "Emails",
     * )
     *
     * @Route(path="/emails/{id}", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fetchAction()
    {
        return $this->action("action.email.fetch");
    }
}