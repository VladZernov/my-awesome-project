<?php

namespace AppBundle\Repository;

use Requestum\ApiBundle\Repository\ApiRepositoryTrait;
use AppBundle\Entity\ViewingStage;

/**
 * ViewingStageRepository
 */
class ViewingStageRepository extends ApiRepository
{
    use ApiRepositoryTrait;

    public function updateSuccessStage()
    {
        $qb = $this->createQueryBuilder('s');

        /** @var ViewingStage $successStage */
        $successStage = $qb
            ->where($qb->expr()->eq('s.flowFlag', $qb->expr()->literal(ViewingStage::FINISH_STAGE)))
            ->getQuery()
            ->getOneOrNullResult();

        if ($successStage) {
            $successStage->setFlowFlag(ViewingStage::MIDDLE_STAGE);
            $this->_em->persist($successStage);
        }
    }

    /**
     * @param ViewingStage $replaceStage
     * @param int $from
     * @param int $to
     */
    public function rearrangeStages(ViewingStage $replaceStage, int $from, int $to)
    {
        $stages = $this->findAll();

        if ($from < $to) {

            /** @var ViewingStage $stage */
            foreach($stages as $stage) {

                if ($stage->getOrder() > $from && $stage->getOrder() <= $to && $stage->getId() !== $replaceStage->getId()) {

                    $stage->decOrder();
                }
            }
        } elseif ($from > $to) {

            /** @var ViewingStage $stage */
            foreach($stages as $stage) {

                if ($stage->getOrder() < $from && $stage->getOrder() >= $to && $stage->getId() !== $replaceStage->getId()) {

                    $stage->incOrder();
                }
            }
        }

    }

    public function getHighestOrderStage()
    {
       return $this->createQueryBuilder('e')
           ->orderBy('e.order', 'DESC')
           ->setMaxResults(1)
           ->getQuery()
           ->getOneOrNullResult();
    }

}
