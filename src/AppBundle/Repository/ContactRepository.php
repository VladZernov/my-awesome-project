<?php

namespace AppBundle\Repository;

use Requestum\ApiBundle\Filter\Handler\SearchHandler;

/**
 * ContactRepository
 */
class ContactRepository extends ApiRepository
{
    /**
     * {@inheritdoc}
     */
    protected function createHandlers()
    {
        return [
            new SearchHandler([
                'firstName',
                'lastName',
            ]),
        ];
    }

    protected function getPathAliases()
    {
        return [
            'group_name' => "[groups][name]",
            'campaign_source_name' => '[campaignSource][name]',
            'unit_name' => '[currentLeasing][unit][name]',
            'property_name' => '[currentLeasing][unit][property][name]',
        ];
    }
}
