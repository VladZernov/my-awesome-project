<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Unit;
use AppBundle\Filter\Handler\Unit\UnitAvailabilityHandler;
use AppBundle\Filter\Handler\Unit\UnitLeasingsDateHandler;
use AppBundle\Filter\Handler\Unit\UnitPeriodAvailabilityHandler;
use Requestum\ApiBundle\Filter\Handler\SearchHandler;

/**
 * UnitRepository
 */
class UnitRepository extends ApiRepository
{

    /**
     * @param string $from
     * @param string $to
     *
     * @return array
     */
    public function getAvailabilitySchedule($from, $to)
    {
        if (!$from || !$to) {
            return [];
        }

        $fromDate = \DateTime::createFromFormat('Y.m', $from);
        $toDate = \DateTime::createFromFormat('Y.m', $to);

        $units = $this->createQueryBuilder('u')
            ->leftJoin('u.leasings', 'l')
            ->andWhere('l.startDate < :to AND l.endDate >= :from')
            ->setParameter('from', $fromDate->format('Y-m-01'))
            ->setParameter('to', $toDate->format('Y-m-t'))
            ->getQuery()
            ->getResult()
        ;

        $stats = [];

        for($date = $fromDate; $date <= $toDate; $date->modify('+1 month')) {

            $stats[$date->format('Y.m')]['gaps'] = 0;
            $stats[$date->format('Y.m')]['expiry'] = 0;

            /** @var Unit $unit */
            foreach ($units as $unit) {
                $stats[$date->format('Y.m')]['gaps'] += $unit->isGapPeriod($date);
                $stats[$date->format('Y.m')]['expiry'] += $unit->isExpiryPeriod($date);
            }
        }

        return $stats;
    }

    /**
     * {@inheritdoc}
     */
    protected function createHandlers()
    {
        return [
            new UnitAvailabilityHandler(),
            new UnitPeriodAvailabilityHandler(),
            new UnitLeasingsDateHandler(),
            new SearchHandler([
                'name',
                'property.name',
                'property.address',
                'property.city',
            ]),
        ];
    }
}
