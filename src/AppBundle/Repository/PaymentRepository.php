<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentType;
use Requestum\ApiBundle\Filter\Handler\SearchHandler;

/**
 * PaymentRepository
 */
class PaymentRepository extends ApiRepository
{
    /**
     * @return mixed
     */
    public function getPaymentsToRemind()
    {
        $remindPeriod = Payment::REMIND_PERIOD;
        $remindDate = (new \DateTime("+$remindPeriod day"))->setTime(0, 0, 0);

        return $this->createQueryBuilder('p')
            ->innerJoin('p.type', 't')
            ->andWhere("p.dueDate = :remindDate AND (p.paid = false OR p.paid is null) AND t.name = :rent")
            ->setParameter('remindDate',  $remindDate)
            ->setParameter('rent', PaymentType::RENT_TYPE)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function getPaymentsToPay()
    {
        $today = (new \DateTime())->setTime(0, 0, 0);

        return $this->createQueryBuilder('p')
            ->innerJoin('p.type', 't')
            ->andWhere("p.dueDate = :today AND (p.paid = false OR p.paid is null) AND t.name = :rent")
            ->setParameter('today', $today)
            ->setParameter('rent', PaymentType::RENT_TYPE)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function getLatePayments()
    {
        $latePeriod = Payment::LATE_PERIOD;
        $lateDate = (new \DateTime("-$latePeriod day"))->setTime(0, 0, 0);

        return $this->createQueryBuilder('p')
            ->innerJoin('p.type', 't')
            ->andWhere("p.dueDate = :lateDate  AND (p.paid = false OR p.paid is null) AND t.name = :rent")
            ->setParameter('lateDate',  $lateDate)
            ->setParameter('rent', PaymentType::RENT_TYPE)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function recalculateOutdatedPaymentsAmounts()
    {
        $penaltyPeriod = Payment::PENALTY_PERIOD;

        return $this->createQueryBuilder('p')
            ->update(Payment::class, 'p')
            ->set('p.amount', 'p.amount * (1 + 0.05 * FLOOR(DATE_DIFF(CURRENT_DATE(), p.dueDate) / :penaltyPeriod))')
            ->set('p.outdated', true)
            ->where('p.dueDate < :today AND p.paid = false')
            ->setParameter(':today',  new \DateTime())
            ->setParameter(':penaltyPeriod', $penaltyPeriod)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    protected function createHandlers()
    {
        return [
            new SearchHandler([
                'contact.firstName',
                'contact.lastName',
                'leasing.unit.property.name',
                'leasing.unit.property.address',
            ]),
        ];
    }

    /**
     * @return array
     */
    protected function getPathAliases()
    {
        return [
            'payment_type' => "[type][name]",
        ];
    }
}
