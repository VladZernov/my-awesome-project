<?php

namespace AppBundle\Repository;

use AppBundle\Filter\Handler\Property\PropertyLeasingsDateHandler;
use Requestum\ApiBundle\Filter\Handler\SearchHandler;

/**
 * PropertyRepository
 */
class PropertyRepository extends ApiRepository
{
    /**
     * {@inheritdoc}
     */
    protected function createHandlers()
    {
        return [
            new PropertyLeasingsDateHandler(),
            new SearchHandler([
                'name',
                'address',
                'units.currentLeasing.contact.email',
                'units.currentLeasing.contact.lastName',
                'units.currentLeasing.contact.firstName',
                'units.name',
            ]),
        ];
    }

// units,units.leasings,units.currentLeasing.contact&contact_email=*1*&contact_first_name=*1*&contact_last_name=*1*&unit_name=*1*&leasings_date[from]=2018.01&leasings_date[to]=2019.01

//    protected function getPathAliases()
//    {
//        return [
//            'contact_email' => "[units][currentLeasing][contact][email]",
//            'contact_last_name' => '[units][currentLeasing][contact][lastName]',
//            'contact_first_name' => '[units][currentLeasing][contact][firstName]',
//            'unit_name' => '[units][name]',
//        ];
//    }
}
