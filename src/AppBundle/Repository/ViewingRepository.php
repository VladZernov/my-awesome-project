<?php

namespace AppBundle\Repository;

use Requestum\ApiBundle\Repository\ApiRepositoryTrait;

/**
 * ViewingRepository
 */
class ViewingRepository extends ApiRepository
{
    use ApiRepositoryTrait;

    protected function getPathAliases()
    {
        return [
            'property_name' => '[unit][property][name]',
            'contact_name' => '[contact][name]',
        ];
    }
}
