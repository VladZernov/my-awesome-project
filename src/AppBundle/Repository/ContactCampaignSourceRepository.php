<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ViewingStage;

/**
 * ContactCampaignSourceRepository
 */
class ContactCampaignSourceRepository extends ApiRepository
{
    /**
     * @param null $from
     * @param null $to
     * @return array|mixed
     */
    public function getStatisticsForPeriod($from = null, $to = null)
    {

        if ($from && $to) {
            $fromDate = \DateTime::createFromFormat('Y.m.d', $from);
            $toDate = \DateTime::createFromFormat('Y.m.d', $to);
        }

        $viewingBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('COUNT(v1.id)')
            ->from('AppBundle:Viewing', 'v1')
            ->innerJoin('v1.stage', 's1', 'WITH', 's1.flowFlag = :flowFlag')
            ->setParameter('flowFlag', ViewingStage::FINISH_STAGE);

        if ($from && $to) {
            $viewingBuilder
                ->where('v1.updatedAt BETWEEN :startDate AND :endDate')
                ->setParameter('startDate', $fromDate)
                ->setParameter('endDate', $toDate->modify('next day'));
        }

        $total = $viewingBuilder
            ->getQuery()
            ->getSingleScalarResult();

        $campaignSourceBuilder = $this->createQueryBuilder('cs')
            ->addSelect('(COUNT(v2.id) * 100 / :total) AS percentage, SUM(v2.price) AS sum')
            ->innerJoin('cs.contacts', 'c')
            ->innerJoin('c.viewings', 'v2')
            ->innerJoin('v2.stage', 's2', 'WITH', 's2.flowFlag = :flowFlag')
            ->groupBy('cs.id')
            ->setParameter('total', $total)
            ->setParameter('flowFlag', ViewingStage::FINISH_STAGE)
        ;

        if ($from && $to) {
            $campaignSourceBuilder
                ->andWhere('v2.updatedAt BETWEEN :startDate AND :endDate')
                ->setParameter('startDate', $fromDate)
                ->setParameter('endDate', $toDate->modify('next day'));
        }

        return $campaignSourceBuilder->getQuery()->getResult();
    }

}
