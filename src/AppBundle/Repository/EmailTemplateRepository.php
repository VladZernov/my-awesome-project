<?php

namespace AppBundle\Repository;

use AppBundle\Entity\EmailTemplate;

/**
 * EmailTemplateRepository
 */
class EmailTemplateRepository extends ApiRepository
{
    /**
     * @param EmailTemplate $template
     * @return mixed
     */
    public function changeActiveTemplate(EmailTemplate $template)
    {
        $builder = $this->createQueryBuilder('t')
            ->update(EmailTemplate::class, 't')
            ->set('t.active', 'false')
            ->where('t.type = :type')
            ->setParameter(':type', $template->getType());

        if ($template->getId()) {

            $builder
                ->andWhere('t != :template')
                ->setParameter(':template', $template);
        }

        return $builder->getQuery()->execute();
    }
}
