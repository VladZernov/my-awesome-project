<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

/**
 * Trait CurrentLeasing
 *
 */
trait CurrentLeasing
{

    /**
     * @param array $filters
     * @param QueryBuilder|null $builder
     * @return mixed
     */
    public function filter(array $filters, QueryBuilder $builder = null)
    {
        if (!$builder) {
            $builder = $this->createQueryBuilder('e');
        }

        $builder
            ->select('e, cl')
            ->leftJoin('e.currentLeasings', 'cl', Expr\Join::WITH, 'cl.startDate <= :now AND cl.endDate >= :now')
            ->setParameter('now', new \DateTime());

        return parent::filter($filters, $builder);
    }
}