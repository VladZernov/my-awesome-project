<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Leasing;
use AppBundle\Entity\LeasingType;
use AppBundle\Entity\PaymentType;
use Doctrine\DBAL\Statement;
use Doctrine\ORM\Query\Expr\Join;

/**
 * LeasingRepository
 */
class LeasingRepository extends ApiRepository
{
    /**
     * @return mixed
     */
    public function getLeasingsOnCheckIn()
    {
        $checkinInterval = Leasing::CHECKIN_PERIOD;
        $checkinDate = (new \DateTime("+$checkinInterval day"))->setTime(0, 0, 0);

        return $this->createQueryBuilder('l')
            ->andWhere("l.startDate = :checkinDate")
            ->setParameter('checkinDate', $checkinDate)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function getLeasingsOnRenew()
    {
        $renewInterval = Leasing::RENEW_PERIOD;
        $renewDate = (new \DateTime("+$renewInterval day"))->setTime(0, 0, 0);

        return $this->createQueryBuilder('l')
            ->innerJoin('l.type', 't')
            ->andWhere("l.endDate = :renewDate AND t.name = :renew")
            ->setParameter('renewDate',  $renewDate)
            ->setParameter('renew', LeasingType::RENEW_TYPE)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function getLeasingsOnCheckOut()
    {
        $checkoutInterval = Leasing::CHECKOUT_PERIOD;
        $checkoutDate = (new \DateTime("+$checkoutInterval day"))->setTime(0, 0, 0);

        return $this->createQueryBuilder('l')
            ->innerJoin("l.payments", "p")
            ->andWhere("l.endDate = :checkout AND p.paid = false")
            ->setParameter('checkout',  $checkoutDate)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \DateTime $payDate
     * @return mixed
     */
    public function getLeasingsToPay(\DateTime $payDate)
    {
        return $this->createQueryBuilder('l')
            ->leftJoin('l.payments', 'p')
            ->leftJoin('p.type', 't', Join::WITH, 'MONTH(p.dueDate) = :currentMonth AND t.name = :rent')
            ->andWhere("l.payDay = :payDay AND l.endDate > :payDate AND t.id IS NULL")
            ->setParameter('currentMonth', (new \DateTime())->format('m'))
            ->setParameter('payDay',  $payDate->format('d'))
            ->setParameter('payDate',  $payDate)
            ->setParameter('rent', PaymentType::RENT_TYPE)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function getYears()
    {
        $sql = "SELECT DISTINCT
                    YEAR(leasing.start_date) AS year
                FROM
                    leasing 
                UNION DISTINCT SELECT DISTINCT
                    YEAR(leasing.end_date) AS year
                FROM
                    leasing
                ORDER BY year;";

        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}
