<?php

namespace AppBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\UnitOfWork;
use Requestum\ApiBundle\Event\FormActionEvent;
use Symfony\Component\Asset\Packages;

use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\User;
use AppBundle\Entity\Viewing;
use AppBundle\Entity\ViewingStage;
use AppBundle\Service\Email\EmailManager;
use AppBundle\Service\User\UserUtility;

/**
 * Class ViewingListener
 */
class ViewingListener
{

    /**
     * @var EmailManager
     */
    private $emailManager;

    /**
     * @var Packages
     */
    private $assetsManager;

    /**
     * LeasingListener constructor.
     * @param EmailManager $emailManager
     * @param Packages $assetsManager
     */
    public function __construct(EmailManager $emailManager, Packages $assetsManager)
    {
        $this->emailManager = $emailManager;
        $this->assetsManager = $assetsManager;
    }

    /**
     * @param Viewing $viewing
     * @param LifecycleEventArgs $event
     * @throws EntityNotFoundException
     */
    public function prePersist(Viewing $viewing, LifecycleEventArgs $event)
    {
        if ($viewing->getStage()) {
            return;
        }

        $firstStage = $event->getObjectManager()->getRepository(ViewingStage::class)->findOneBy(['flowFlag' => ViewingStage::START_STAGE]);

        if (!$firstStage) {
            throw new EntityNotFoundException('Start stage not found');
        }

        $viewing->setStage($firstStage);
    }


    /**
     * @param FormActionEvent $event
     */
    public function onViewingSucceeding(FormActionEvent $event)
    {
        /** @var Viewing $viewing */
        $viewing = $event->getSubject();

        $manager = $event->getDoctrineRegistry()->getManager();

        /** @var UnitOfWork $uow */
        $uow = $event->getDoctrineRegistry()->getManager()->getUnitOfWork();
        $uow->computeChangeSets();
        $changeSet = $uow->getEntityChangeSet($viewing);

        if (isset($changeSet['stage']) && $viewing->getStage()->isSuccess()) {

            $contact = $viewing->getContact();

            $user = new User();
            $user->setEmail($contact->getEmail());
            $user->setEnabled(true);
            $user->setRole(User::ROLE_USER);
            $user->setFullName($contact->getFullName());
            $user->setUsername($contact->getEmail());
            $user->setPlainPassword(UserUtility::generatePassword());

            $manager->persist($user);

            $parameters =
                [
                    '%full_name%' => $user->getFullName(),
                    '%email%' => $user->getEmail(),
                    '%password%' => $user->getPlainPassword(),
                    '%login_page%' => $this->assetsManager->getUrl('/login'),
                ];

            $this->emailManager->sendByType(EmailTemplate::REGISTRATION_TYPE, $contact, $parameters);
        }
    }
}