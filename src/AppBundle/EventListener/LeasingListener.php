<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\Leasing;
use AppBundle\Service\Email\EmailManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LeasingListener
 */
class LeasingListener
{
    /**
     * @var EmailManager
     */
    private $emailManager;

    /**
     * LeasingListener constructor.
     * @param EmailManager $emailManager
     */
    public function __construct(EmailManager $emailManager)
    {
        $this->emailManager = $emailManager;
    }

    /**
     * @param Leasing $leasing
     * @param LifecycleEventArgs $event
     */
    public function prePersist(Leasing $leasing, LifecycleEventArgs $event)
    {
        $contact = $leasing->getContact();
        $parameters = $leasing->getEmailParameters();
        $this->emailManager->sendByType(EmailTemplate::CHECKIN_TYPE, $contact, $parameters);
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     * @param Leasing $leasing
     * @param LifecycleEventArgs $event
     */
    public function updateCurrentLeasing(Leasing $leasing, LifecycleEventArgs $event)
    {
        if ($leasing->isCurrent()) {
            $leasing->getContact()->setCurrentLeasing($leasing);
            $leasing->getUnit()->setCurrentLeasing($leasing);
        }
    }
}