<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ViewingStage;
use AppBundle\Repository\ViewingStageRepository;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use Requestum\ApiBundle\Event\FormActionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class ViewingStageListener
 */
class ViewingStageListener
{

    /**
     * @param ViewingStage $stage
     * @param LifecycleEventArgs $args
     */
    public function prePersist(ViewingStage $stage, LifecycleEventArgs $args)
    {
        /** @var ViewingStageRepository $repository */
        $repository = $args->getObjectManager()->getRepository(ViewingStage::class);

        /** @var ViewingStage $highestOrderStage */
        $highestOrderStage = $repository->getHighestOrderStage();

        if ($stage->getOrder()) {
            return;
        }

        $highestOrder = 1;

        if ($stage->isSuccess()) {

            $repository->updateSuccessStage();
            $highestOrder = $highestOrderStage ? $highestOrderStage->getOrder() : 0;
            $highestOrder++;
        } elseif ($highestOrderStage) {

            $highestOrder = $highestOrderStage->getOrder();
            $highestOrderStage->incOrder();
        }

        $stage->setOrder($highestOrder);
    }

    /**
     * @param ViewingStage $stage
     * @param LifecycleEventArgs $args
     */
    public function preRemove(ViewingStage $stage, LifecycleEventArgs $args)
    {
        if (!$stage->isMiddle()) {

            throw new BadRequestHttpException('The stage can\'t be removed');
        }
    }

    /**
     * @param FormActionEvent $event
     */
    public function rearrangeStages(FormActionEvent $event)
    {
        $stage = $event->getSubject();

        /** @var ViewingStageRepository $repository */
        $repository = $event->getDoctrineRegistry()->getRepository(ViewingStage::class);

        /** @var UnitOfWork $uow */
        $uow = $event->getDoctrineRegistry()->getManager()->getUnitOfWork();
        $uow->computeChangeSets();
        $changeset = $uow->getEntityChangeSet($stage);

        if (isset($changeset['order'])) {
            $from = $changeset['order'][0];
            $to = $changeset['order'][1];

            $repository->rearrangeStages($stage, $from, $to);
        }
    }
}