<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EmailTemplate;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * Class EmailTemplateListener
 */
class EmailTemplateListener
{
    /**
     * @param EmailTemplate $template
     * @param LifecycleEventArgs $event
     */
    public function prePersist(EmailTemplate $template, LifecycleEventArgs $event)
    {
        $repository = $event->getObjectManager()->getRepository(EmailTemplate::class);

        if ($template->isActive()) {

            $repository->changeActiveTemplate($template);
        }
    }

    /**
     * @param EmailTemplate $template
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(EmailTemplate $template, PreUpdateEventArgs $event)
    {
        $repository = $event->getObjectManager()->getRepository(EmailTemplate::class);

        if ($event->hasChangedField('active') && $template->isActive()) {

            $repository->changeActiveTemplate($template);
        }
    }
}