<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\Payment;
use AppBundle\Service\Email\EmailManager;
use Doctrine\Common\Persistence\Event\PreUpdateEventArgs;

/**
 * Class PaymentListener
 */
class PaymentListener
{

    /**
     * @var EmailManager
     */
    private $emailManager;

    /**
     * LeasingListener constructor.
     * @param EmailManager $emailManager
     */
    public function __construct(EmailManager $emailManager)
    {
        $this->emailManager = $emailManager;
    }

    /**
     * @param Payment $payment
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(Payment $payment, PreUpdateEventArgs $event)
    {
        if ($event->hasChangedField('paid') && $payment->isPaid()) {

            $payment->setPaidDate(new \DateTime());
            $payment->setOutdated($payment->getPaidDate() > $payment->getDueDate());
            $this->emailManager->sendByType(EmailTemplate::RECEIPT_TYPE, $payment->getContact(), $payment->getEmailParameters());
        }
    }
}