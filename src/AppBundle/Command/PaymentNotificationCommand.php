<?php

namespace AppBundle\Command;

use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\Payment;
use AppBundle\Repository\PaymentRepository;
use AppBundle\Service\DocumentGenerator;
use AppBundle\Service\Email\EmailManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentNotificationCommand extends ContainerAwareCommand
{
    /**
     * @var EmailManager
     */
    private $emailManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var DocumentGenerator
     */
    private $documentGenerator;

    protected function configure()
    {
        $this->setName('app:notify:payment')
            ->setDescription('Send email notifications for payments.')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->emailManager = $this->getContainer()->get('app.service.email.email_manager');
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
        $this->documentGenerator = $this->getContainer()->get('app.service.document_generator');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var PaymentRepository $repository */
        $repository = $this->entityManager->getRepository(Payment::class);

        $remindPayments = $repository->getPaymentsToRemind();
        $invoicePayments = $repository->getPaymentsToPay();
        $latePayments = $repository->getLatePayments();

        $notifications = [
            EmailTemplate::REMIND_TYPE => $remindPayments,
            EmailTemplate::INVOICE_TYPE => $invoicePayments,
            EmailTemplate::LATE_TYPE => $latePayments,
        ];

        foreach ($notifications as $type => $payments) {

            /** @var Payment $payment */
            foreach($payments as $payment) {

                $recipient = $payment->getContact();
                $parameters = $payment->getEmailParameters();
                $attachments = [];

                if ($type === EmailTemplate::INVOICE_TYPE) {
                    $attachments[] = $this->documentGenerator->generateRentInvoice($payment);
                }

                $this->emailManager->sendByType($type, $recipient, $parameters, $attachments);
            }
        }
    }
}