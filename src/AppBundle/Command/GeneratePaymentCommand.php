<?php

namespace AppBundle\Command;

use AppBundle\Entity\Leasing;
use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentType;
use AppBundle\Repository\LeasingRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GeneratePaymentCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function configure()
    {
        $this->setName('app:payment:generate')
            ->setDescription('Generate rent payments for current leasings.');
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rentInterval = Payment::RENT_INTERVAL;
        $payDate = new \DateTime("+$rentInterval day");

        /** @var LeasingRepository $leasingRepo */
        $leasingRepo = $this->entityManager->getRepository(Leasing::class);

        $leasings = $leasingRepo->getLeasingsToPay($payDate);

        /** @var PaymentType $rentType */
        $rentType = $this->entityManager->getRepository(PaymentType::class)->findOneBy(['name' => PaymentType::RENT_TYPE]);

        /** @var Leasing $leasing */
        foreach ($leasings as $leasing) {

            $monthRate = $leasing->getPrice();
            $amount = $leasing->isLastMonth() ? $monthRate * (12/365) * $leasing->getEndDate()->format('d') : $monthRate;

            $payment = new Payment();
            $payment->setContact($leasing->getContact());
            $payment->setType($rentType);
            $payment->setDueDate($payDate);
            $payment->setTransactionType(Payment::TRANSACTION_CREDIT);
            $payment->setLeasing($leasing);
            $payment->setAmount($amount);

            $this->entityManager->persist($payment);
        }

        $this->entityManager->flush();

    }
}