<?php

namespace AppBundle\Command;

use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\Leasing;
use AppBundle\Repository\LeasingRepository;
use AppBundle\Service\Email\EmailManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LeasingNotificationCommand extends ContainerAwareCommand
{
    /**
     * @var EmailManager
     */
    private $emailManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function configure()
    {
        $this->setName('app:notify:leasing')
            ->setDescription('Send email notifications for leasings.')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->emailManager = $this->getContainer()->get('app.service.email.email_manager');
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var LeasingRepository $repository */
        $repository = $this->entityManager->getRepository(Leasing::class);

        $checkinLeasings = $repository->getLeasingsOnCheckIn();
        $renewLeasings = $repository->getLeasingsOnRenew();
        $checkoutLeasings = $repository->getLeasingsOnCheckOut();

        $notifications = [
            EmailTemplate::CHECKIN_REMIND_TYPE => $checkinLeasings,
            EmailTemplate::RENEW_TYPE => $renewLeasings,
            EmailTemplate::CHECKOUT_TYPE => $checkoutLeasings,
        ];

        foreach ($notifications as $type => $leasings) {

            /** @var Leasing $leasing */
            foreach($leasings as $leasing) {

                $recipient = $leasing->getContact();
                $parameters = $leasing->getEmailParameters();
                $this->emailManager->sendByType($type, $recipient, $parameters);
            }
        }
    }
}