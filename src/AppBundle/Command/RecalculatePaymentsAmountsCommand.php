<?php

namespace AppBundle\Command;

use AppBundle\Entity\Payment;
use AppBundle\Repository\PaymentRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RecalculatePaymentsAmountsCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function configure()
    {
        $this->setName('app:payment:recalculate')
            ->setDescription('Recalculate rent payments.');
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var PaymentRepository $repository */
        $repository = $this->entityManager->getRepository(Payment::class);

        $repository->recalculateOutdatedPaymentsAmounts();
    }
}