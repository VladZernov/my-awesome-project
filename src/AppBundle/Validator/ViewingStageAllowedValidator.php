<?php

namespace AppBundle\Validator;

use AppBundle\Entity\ViewingStage;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ViewingStageAllowedValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * TemplateActiveValidator constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ViewingStage $viewingStage
     * @param Constraint $constraint
     */
    public function validate($viewingStage, Constraint $constraint)
    {

        $lastStageOrder = $this->entityManager->getRepository(ViewingStage::class)->createQueryBuilder('s')
            ->select('MAX(s.order)')
            ->getQuery()
            ->getSingleScalarResult();

        if(($viewingStage->isStart() && $viewingStage->getOrder() != 1) ||
            ($viewingStage->isSuccess() && $viewingStage->getOrder() != $lastStageOrder)) {

            $this->context->buildViolation('You can\'t move first and last stages')
                ->addViolation();

        } elseif($viewingStage->getOrder() == 1 || $viewingStage->getOrder() >= $lastStageOrder) {

            $this->context->buildViolation('Custom stages cannot be placed before "Scheduled" and after the "Contract Won"')
                ->addViolation();
        }
    }
}