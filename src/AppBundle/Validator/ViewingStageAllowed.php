<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ViewingStageAllowed extends Constraint
{

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
