<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class LeasingAllowed extends Constraint
{
    public $message = 'The contact hasn\'t any successfully closed viewings related to the unit';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
