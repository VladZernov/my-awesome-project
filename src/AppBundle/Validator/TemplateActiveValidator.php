<?php

namespace AppBundle\Validator;

use AppBundle\Entity\EmailTemplate;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TemplateActiveValidator extends ConstraintValidator
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * TemplateActiveValidator constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $template
     * @param Constraint $constraint
     */
    public function validate($template, Constraint $constraint)
    {
        $activeCount = $this->entityManager->getRepository(EmailTemplate::class)
            ->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.type = :type AND t.id != :id AND t.active = :active')
            ->setParameter('type', $template->getType())
            ->setParameter('active', true)
            ->setParameter('id', $template->getId())
            ->getQuery()
            ->getSingleScalarResult();

        if ($activeCount == 0 && !$template->isActive()) {

            $this->context->buildViolation('Must be at least one active template for type "{{ type }}"')
                ->setParameter('{{ type }}', $template->getType())
                ->addViolation();
        } elseif ($activeCount > 1) {

            $this->context->buildViolation('Too many active templates for type "{{ type }}"')
                ->setParameter('{{ type }}', $template->getType())
                ->addViolation();
        }
    }
}