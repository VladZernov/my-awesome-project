<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ViewingAllowed extends Constraint
{
    public $message = 'Viewing can not be assigned to the date';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
