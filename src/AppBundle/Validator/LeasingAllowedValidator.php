<?php

namespace AppBundle\Validator;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Unit;
use AppBundle\Entity\Viewing;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LeasingAllowedValidator extends ConstraintValidator
{
    public function validate($leasing, Constraint $constraint)
    {

        /** @var Contact $contact */
        $contact = $leasing->getContact();

        if (!$contact) {
            $this->context->buildViolation('Contact not found.')
                ->atPath('contact')
                ->addViolation();
        }

        $expr = Criteria::expr();
        $periodCriteria = new Criteria();
        $periodCriteria
            ->where($expr->andX($expr->gt('endDate', $leasing->getStartDate()),
                $expr->lt('startDate', $leasing->getEndDate()),
            $expr->neq('id', $leasing->getId())));

        $intersectLeasing = $contact->getLeasings()->matching($periodCriteria)->first() ?: null;

        if ($intersectLeasing) {
            $this->context->buildViolation('The contact already has leasing for provided period.')
                ->atPath('contact')
                ->addViolation();
        }

        /** @var Unit $unit */
        $unit = $leasing->getUnit();

        if (!$unit) {
            $this->context->buildViolation('Unit not found.')
                ->atPath('unit')
                ->addViolation();
        }

        $unitLeasing = $unit->getLeasings()->matching($periodCriteria)->first() ?: null;

        if ($unitLeasing) {
            $this->context->buildViolation('The unit already has leasing for provided period.')
                ->atPath('unit')
                ->addViolation();
        }

        $successViewing = $contact->getViewings()->filter(function(Viewing $element) use ($unit) {
            return $element->getStage()->isSuccess() && $element->getUnit() === $unit;
        });

        if(!$successViewing) {

            $this->context->buildViolation($constraint->message)
                ->atPath('contact')
                ->addViolation();
        }
    }
}