<?php

namespace AppBundle\Service;

use AppBundle\Entity\File;
use AppBundle\Entity\Payment;
use Knp\Snappy\Pdf;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class DocumentGenerator
 * @package AppBundle\Service
 */
class DocumentGenerator
{

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @var Pdf
     */
    protected $pdfGenerator;

    /**
     * DocumentGenerator constructor.
     *
     * @param EngineInterface $templating
     * @param string $rootDir
     * @param Pdf $pdfGenerator
     */
    public function __construct(EngineInterface $templating, string $rootDir, Pdf $pdfGenerator)
    {
        $this->templating = $templating;
        $this->rootDir = $rootDir;
        $this->pdfGenerator = $pdfGenerator;
    }

    /**
     * @param Payment $payment
     * @return File
     */
    public function generateRentInvoice(Payment $payment)
    {
        $invoiceName = 'invoice_'.bin2hex(random_bytes(10)).'.pdf';
        $invoicePath = $this->rootDir.'/../web/docs/'.$invoiceName;

        $this->pdfGenerator->generateFromHtml(
            $this->templating->render(
                'AppBundle:Emails:invoice.html.twig',
                [
                    'payment' => $payment,
                ]
            ),
            $invoicePath
        );

        $invoice = new File();
        $invoice->setName($invoiceName);
        $invoice->setPath($invoicePath);
        $invoice->setContentType('application/pdf');

        return $invoice;
    }
}