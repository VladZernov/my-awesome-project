<?php

namespace AppBundle\Service\Email;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Email;
use AppBundle\Entity\EmailRecipient;
use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Requestum\EmailSenderBundle\Service\EmailSender;

/**
 * Class EmailManager
 * @package AppBundle\Service\Email
 */
class EmailManager
{
    /**
     * @var EmailSender
     */
    protected $emailSender;

    /**
     * @var string
     */
    protected $defaultSenderEmail;

    /**
     * @var string
     */
    protected $defaultBcc;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * EmailManager constructor.
     * @param EmailSender $emailSender
     * @param mixed $defaultSenderEmail
     * @param mixed $defaultBcc
     * @param EntityManager $entityManager
     */
    public function __construct(EmailSender $emailSender, $defaultSenderEmail, $defaultBcc, EntityManager $entityManager)
    {
        $this->emailSender = $emailSender;
        $this->defaultSenderEmail = $defaultSenderEmail;
        $this->defaultBcc = $defaultBcc;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $type
     * @param Contact $recipient
     * @param array $parameters
     * @param array $attachments
     */
    public function sendByType(string $type, Contact $recipient, array $parameters, array $attachments = [])
    {
        /** @var EmailTemplate $emailTemplate */
        $emailTemplate = $this->entityManager->getRepository(EmailTemplate::class)->findOneBy(['type' => $type]);

        if (!$emailTemplate) {
            throw new \InvalidArgumentException('Email template with corresponded type not found');
        }

        $message = $this->buildMessage($emailTemplate->getContent(), $parameters);

        $email = new Email();
        $email->setSubject($emailTemplate->getSubject());
        $email->setMessage($message);
        $email->addRecipient(new EmailRecipient($recipient));

        $emailAttachments = new ArrayCollection(
            array_merge($emailTemplate->getAttachments()->toArray(), $attachments)
        );

        $email->setAttachments($emailAttachments);

        $this->sendEmail($email);

        $this->entityManager->persist($email);
        $this->entityManager->flush();
    }

    /**
     * @param Email $email
     */
    public function sendEmail(Email $email)
    {
        $message = \Swift_Message::newInstance();

        $emails = $email->getRecipientsEmails();

        $message->setFrom($this->defaultSenderEmail);
        $message->setTo($emails);
        $message->addBcc($this->defaultBcc);
        $message->setSubject($email->getSubject());
        $message->setBody($email->getMessage(), 'text/html');

        /** @var File $attachment */
        foreach($email->getAttachments() as $attachment) {
            $message->attach(\Swift_Attachment::fromPath($attachment->getPath()));
        }

        $this->emailSender->sendMessage($message);
    }

    /**
     * @param string $content
     * @param array $parameters
     * @return mixed|string
     */
    private function buildMessage(string $content, array $parameters)
    {
        $content = strtr($content, $parameters);

        $orphanParam = preg_match('/%\w+%/', $content);

        if ($orphanParam) {
            throw new \InvalidArgumentException('Wrong email template/parameters.');
        }

        return $content;
    }
}