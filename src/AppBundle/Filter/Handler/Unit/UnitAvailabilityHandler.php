<?php

namespace AppBundle\Filter\Handler\Unit;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Requestum\ApiBundle\Filter\Handler\AbstractByNameHandler;

class UnitAvailabilityHandler extends AbstractByNameHandler
{
    /**
     * @param QueryBuilder $builder
     * @param string $filter
     * @param mixed $value
     */
    public function handle(QueryBuilder $builder, $filter, $value)
    {
        if ($value) {
            $builder
                ->leftJoin(sprintf('%s.leasings', $builder->getRootAliases()[0]), 'l')
                ->andWhere('l.endDate < :today OR l.id IS NULL')
                ->setParameter('today', new \DateTime());
        } else {
            $builder
                ->innerJoin(sprintf('%s.leasings', $builder->getRootAliases()[0]), 'l', Join::WITH, 'l.endDate >= :today')
                ->setParameter('today', new \DateTime());
        }
    }

    /**
     * @return string
     */
    protected function getFilterKey()
    {
        return "available";
    }
}