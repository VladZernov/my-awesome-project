<?php

namespace AppBundle\Filter\Handler\Unit;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Requestum\ApiBundle\Filter\Handler\AbstractByNameHandler;

class UnitPeriodAvailabilityHandler extends AbstractByNameHandler
{
    /**
     * @param QueryBuilder $builder
     * @param string $filter
     * @param mixed $period
     * @throws \Exception
     */
    public function handle(QueryBuilder $builder, $filter, $period)
    {

        $fromDate = new \DateTime();
        $toDate = new \DateTime();

        if (!empty($period['from'])) {
            $fromDate = \DateTime::createFromFormat('Y.m', $period['from']);
        }

        if (!$fromDate) {
            throw new \Exception('Wrong format of date parameter from');
        }

        if (!empty($period['to'])) {
            $toDate = \DateTime::createFromFormat('Y.m', $period['to']);
        }

        if (!$toDate) {
            throw new \Exception('Wrong format of date parameter to');
        }

        $builder
            ->leftJoin(sprintf('%s.leasings', $builder->getRootAliases()[0]), 'l')
            ->andWhere('NOT(l.startDate <= :to AND l.endDate >= :from) OR l.id IS NULL')
            ->setParameter('from', $fromDate->format('Y.m.01'))
            ->setParameter('to', $toDate->format('Y.m.t'));

    }

    /**
     * @return string
     */
    protected function getFilterKey()
    {
        return "available_on";
    }
}