<?php

namespace AppBundle\Filter\Handler\Unit;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Requestum\ApiBundle\Filter\Handler\AbstractByNameHandler;

class UnitLeasingsDateHandler extends AbstractByNameHandler
{
    /**
     * @param QueryBuilder $builder
     * @param string $filter
     * @param mixed $period
     */
    public function handle(QueryBuilder $builder, $filter, $period)
    {
        $fromDate = \DateTime::createFromFormat('Y.m', $period['from']);
        $toDate = \DateTime::createFromFormat('Y.m', $period['to']);

        $builder
            ->addSelect('l')
            ->leftJoin(sprintf('%s.leasings', $builder->getRootAliases()[0]), 'l', Join::WITH, 'l.startDate <= :to AND l.endDate >= :from')
            ->setParameter('from', $fromDate->format('Y-m-01'))
            ->setParameter('to', $toDate->format('Y-m-t'));
    }

    /**
     * @return string
     */
    protected function getFilterKey()
    {
        return "leasings_date";
    }
}