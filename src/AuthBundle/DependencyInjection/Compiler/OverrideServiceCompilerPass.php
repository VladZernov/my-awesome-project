<?php

namespace AuthBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use AuthBundle\Entity\ClientManager;

/**
 * Class OverrideServiceCompilerPass
 *
 * @package MaxTvMedia\AuthBundle\DependencyInjection\Compiler
 */
class OverrideServiceCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('fos_oauth_server.client_manager.default');
        $definition
            ->setClass(ClientManager::class)
        ;
    }
}