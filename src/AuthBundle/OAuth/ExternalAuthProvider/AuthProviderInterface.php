<?php

namespace AuthBundle\OAuth\ExternalAuthProvider;

/**
 * Interface ExternalAuthProviderInterface
 */
interface AuthProviderInterface
{
    /**
     * @param mixed $token
     * @return UserInfo
     */
    public function getUserInfo($token);
}