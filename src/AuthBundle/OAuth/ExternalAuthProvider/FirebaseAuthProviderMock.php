<?php

namespace AuthBundle\OAuth\ExternalAuthProvider;

/**
 * Class FirebaseAuthProviderMock
 */
class FirebaseAuthProviderMock implements AuthProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getUserInfo($token)
    {
        $result = new UserInfo();
        $result->id = 'mock_firebase_id';

        return $result;
    }
}