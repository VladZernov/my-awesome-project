<?php

namespace AuthBundle\OAuth\ExternalAuthProvider;

use AuthBundle\OAuth\ExternalAuthProvider\Exception\InvalidAccessTokenException;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

/**
 * Class FirebaseAuthProvider
 */
class FirebaseAuthProvider implements AuthProviderInterface
{
    /**
     * @var \Kreait\Firebase
     */
    protected $sdk;

    /**
     * FirebaseAuthProvider constructor.
     * @param mixed $config
     */
    public function __construct($config)
    {
        $this->sdk = (new Factory())
            ->withServiceAccount(ServiceAccount::fromArray($config))
            ->create()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserInfo($token)
    {
        try {
            $verifiedToken = $this->sdk->getAuth()->verifyIdToken($token);
        } catch (InvalidToken $exception) {
            throw new InvalidAccessTokenException();
        }

        $result = new UserInfo();
        $result->id = $verifiedToken->getClaim('sub');

        return $result;
    }
}