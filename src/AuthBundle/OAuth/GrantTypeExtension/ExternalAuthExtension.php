<?php

namespace AuthBundle\OAuth\GrantTypeExtension;

use AppBundle\Entity\User;
use AuthBundle\OAuth\ExternalAuthProvider\AuthProviderInterface;
use AuthBundle\OAuth\ExternalAuthProvider\Exception\InvalidAccessTokenException;
use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Storage\GrantExtensionInterface;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2;
use OAuth2\OAuth2ServerException;
use Requestum\ApiBundle\Util\ErrorFactory;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ExternalAuthExtension implements GrantExtensionInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var ErrorFactory
     */
    protected $errorFactory;

    /**
     * @var PropertyAccessor
     */
    protected $accessor;

    /**
     * @var AuthProviderInterface[]
     */
    protected $providers;

    /**
     * SocialGrantExtension constructor.
     * @param EntityManager $entityManager
     * @param ValidatorInterface $validator
     * @param ErrorFactory $errorFactory
     * @param AuthProviderInterface[] $providers
     */
    public function __construct(EntityManager $entityManager, ValidatorInterface $validator, ErrorFactory $errorFactory, array $providers = [])
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->errorFactory = $errorFactory;
        $this->accessor = PropertyAccess::createPropertyAccessor();

        foreach ($providers as $name => $provider) {
            $this->addProvider($name, $provider);
        }
    }

    /**
     * @param mixed $name
     * @param AuthProviderInterface $provider
     */
    public function addProvider($name, AuthProviderInterface $provider)
    {
        $this->providers[$name] = $provider;
    }

    /**
     * @param IOAuth2Client $client
     * @param array $inputData
     * @param array $authHeaders
     * @return array|bool
     * @throws OAuth2ServerException
     */
    public function checkGrantExtension(IOAuth2Client $client, array $inputData, array $authHeaders)
    {
        if (!isset($this->providers[$inputData['service']])) {
            throw new OAuth2ServerException(OAuth2::HTTP_BAD_REQUEST, "unsupported_service");
        }

        try {
            $userInfo = $this->providers[$inputData['service']]->getUserInfo($inputData['token']);
        } catch (InvalidAccessTokenException $exception) {
            return false;
        }

        $repository = $this->entityManager->getRepository(User::class);

        if (!$user = $repository->findOneBy(['externalId' => $userInfo->id, 'externalService' => $inputData['service']])) {
            $user = new User();
            $user->setExternalId($userInfo->id);
            $user->setExternalService($inputData['service']);

            /** @var ConstraintViolation[] $constraints */
            $constraints = $this->validator->validate($user, null, ['Social']);

            if (count($constraints)) {
                $errors = [];

                foreach ($constraints as $constraint) {
                    $this->accessor->setValue(
                        $errors,
                        '['.str_replace('.', '][', $constraint->getPropertyPath()).']',
                        $constraint->getMessage()
                    );
                }

                throw new OAuth2ServerException(OAuth2::HTTP_BAD_REQUEST, "unprocessable_user", $errors);
            }

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return ['data' => $user];
    }
}