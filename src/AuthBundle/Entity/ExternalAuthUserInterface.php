<?php

namespace AuthBundle\Entity;

interface ExternalAuthUserInterface
{
    /**
     * @param mixed $id
     * @return mixed
     */
    public function setExternalId($id);

    /**
     * @param mixed $service
     * @return mixed
     */
    public function setExternalService($service);
}