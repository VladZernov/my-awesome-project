<?php

namespace AuthBundle\Entity;

/**
 * Class ClientManager
 *
 * @package AuthBundle\Security
 */
class ClientManager extends \FOS\OAuthServerBundle\Entity\ClientManager
{
    /**
     * {@inheritdoc}
     */
    public function findClientByPublicId($publicId)
    {
        return $this->findClientBy(array(
            'randomId' => $publicId,
        ));
    }
}