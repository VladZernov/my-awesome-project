'use strict';

require('dotenv').config();

let gulp = require('gulp');
let replace = require('gulp-replace');
let clean = require('gulp-clean');
let docsBuilder = require('api-console-builder');

let path = {
    build: {
        spec: './../web/docs/spec/',
        web: './../web/docs/sandbox/'
    },
    src: {
        spec: './spec/**/*'
    },
    clean: './build'
};

gulp.task('clean', function (cb) {
    return gulp.src(path.clean).pipe(clean());
});

gulp.task('build-spec', ['clean'], function (cb) {
    return gulp.src(path.src.spec)
        .pipe(replace('{{ BASE_URI }}', process.env.API_BASE_URI))
        .pipe(gulp.dest(path.build.spec))

});

gulp.task('build-sandbox', ['build-spec'], function (cb) {
    docsBuilder({
        src: 'https://github.com/mulesoft/api-console/archive/v4.2.1.zip',
        dest: path.build.web,
        raml: path.build.spec+'api.raml',
        useJson: true,
        verbose: true
    }).then(function () {
        cb();
    }).catch(function () {
        cb('docs app build failed');
    });
});

gulp.task('build', ['clean', 'build-spec', 'build-sandbox']);
