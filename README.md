Tenafy
===================
1. Create database
2. git clone git@gitlab.requestum.com:silcki/tenafy-back.git
3. Run `composer install` (https://getcomposer.org/download/)
4. Create database schema: `php bin/console doctrine:schema:create`
5. Load fixtures `php bin/console app:fixtures:load`
6. In Vhost of Apache or Nginx base dir should leads to {PATH}/web
7. `cp web/.htaccess.dist web/.htaccess`
8. Run bin/phpunit

RAML
===================
1. `cp docs/.env.dist docs/.env`
2. Set `API_BASE_URI` to URL of current environment
3. Run `npm install`
4. Run `gulp build-sandbox` 